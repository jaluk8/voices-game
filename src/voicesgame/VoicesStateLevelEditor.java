package voicesgame;

import java.util.LinkedList;
import java.util.Stack;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesStateLevelEditor extends VoicesLevelBasedState
{
	public final float SCROLL_SPEED = 2;
	public final double SCROLL_BORDER = 0.01;
	
	public final int MODE_NORMAL = 0;
	public final int MODE_MOVING = 1;
	
	private VoicesButton playButton;
	private VoicesButton backButton;
	private VoicesButton pickLvlButton;
	private VoicesButton pickTmpButton;
	private VoicesButton undoButton;
	private VoicesButton redoButton;
	private VoicesButton pickSpecialButton;

	private VoicesTextureObject preview;
		
	private LinkedList<VoicesLevelTemplate> levelHistory = new LinkedList<VoicesLevelTemplate>();
	private int historyIndex = 0;
	
	private boolean moveModeCooldown = false;
	
	public VoicesStateLevelEditor(int s)
	{
		super(s, VoicesGame.STATE_LEVEL_PICKER);
		
		playButton        = new VoicesButton(new Vector2f(4, 4),    new Vector2f(252, 28),  "play",        Color.black,   "Test");
		backButton        = new VoicesButton(new Vector2f(4, 36),   new Vector2f(252, 28),  "back",        Color.cyan,    "Back");
		pickSpecialButton = new VoicesButton(new Vector2f(260, 4),  new Vector2f(252, 28),  "pickSpecial", Color.magenta, "Pick Entity");
		pickTmpButton     = new VoicesButton(new Vector2f(260, 36),  new Vector2f(252, 28),  "pickTmp",     Color.pink,    "Pick Obstacle");
		pickLvlButton     = new VoicesButton(new Vector2f(516, 4),  new Vector2f(252, 28),  "pickLvl",     Color.green,   "Pick Level");
		
		undoButton        = new VoicesButton(new Vector2f(772, 4),  new Vector2f(252, 28),  "undo",        Color.blue,    "Undo");
		redoButton        = new VoicesButton(new Vector2f(772, 36), new Vector2f(252, 28),  "redo",        Color.orange,  "Redo");

		addMenuObject(playButton);
		addMenuObject(backButton);
		addMenuObject(pickLvlButton);
		addMenuObject(pickTmpButton);
		addMenuObject(undoButton);
		addMenuObject(redoButton);
		addMenuObject(pickSpecialButton);
	}

	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
	{	
		for (VoicesMenuObject o : menuObjects)
		{
			o.rescaleVirtualVectors();
		}
	}

	public void setMode(int aMode)
	{
		if (mode == MODE_MOVING)
		{
			moveModeCooldown = true;
		}
		mode = aMode;
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException
	{
		super.render(gc, sbg, g);
		if(preview != null)
		{
			preview.render(gc, sbg, g, true);
		}
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException
	{
		super.update(gc, sbg, delta);
		if(preview != null)
		{
			preview.update(gc, sbg, delta);
		}
		
		updateEvents();
		moveModeCooldown = false;
		
		if (level != null)
		{
			updateCamera(delta);
		}
	}
	
	private void updateCamera(int delta)
	{
		int x = Mouse.getX();
		int y = Mouse.getY();
		Vector2f scroll = VoicesGameBoard.getInstance().getScroll();
		double xBorder = Display.getWidth() * SCROLL_BORDER;
		double yBorder = Display.getHeight() * SCROLL_BORDER;
		
		if (x > Display.getWidth() - xBorder)
		{
			scroll.x -= SCROLL_SPEED * delta;
		}
		else if (x < xBorder && scroll.x < 0)
		{
			scroll.x += SCROLL_SPEED * delta;
		}
		if (y > Display.getHeight() - yBorder)
		{
			scroll.y -= SCROLL_SPEED * delta;
		}
		else if (y < yBorder && scroll.y < 0)
		{
			scroll.y += SCROLL_SPEED * delta;
		}
		
		VoicesGameBoard.getInstance().setScroll(scroll);
	}

	public void updateMenuEvents(Stack<VoicesMenuEvent> s)
	{
		while (s.size() > 0)
		{
			VoicesMenuEvent e = s.pop();
			VoicesMenuObject source = e.getMenuObject();
			
			switch (source.getName())
			{
			case "play":
				save();
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_PLAY, VoicesStatePlay.MODE_TESTING);
				events.clear();
				break;
			case "back":
				save();
				try
				{
					VoicesGameBoard.getInstance().setLevelTemplate(null);
				}
				catch (SlickException e1)
				{
					e1.printStackTrace();
				}
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_MAIN_MENU);
				events.clear();
				break;
			case "pickLvl":
				save();
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_PICKER);
				events.clear();
				break;
			case "pickTmp":
				save();
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_TEXTURE_OBJECT_PICKER);
				events.clear();
				break;
			case "pickSpecial":
				save();
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_SPECIAL_PICKER);
				break;
			case "undo":
				undo();
				events.clear();
				break;
			case "redo":
				redo();
				events.clear();
				break;
			}
		}
	}
	
	public void updateEditorEvents(Stack<VoicesEditorEvent> s)
	{
		while(s.size() > 0)
		{
			VoicesEditorEvent event = s.pop();
			VoicesTextureObject source = event.getTextureObject();
			int method = event.getEvent();
			
			if (method == VoicesEditorEvent.METHOD_ADD && source == preview && source.getClass().getName() != VoicesPlayer.class.getName())
			{
				VoicesTextureObject o = source.makeCopy();
				o.setMode(VoicesObstacle.MODE_IN_EDITOR);
				level.addTextureObject(o);
				events.clear();
				this.writeHistory();
				return;
			}
			else if (method == VoicesEditorEvent.METHOD_DELETE && source != preview  && source.getClass().getName() != VoicesPlayer.class.getName())
			{
				level.getTextureObjects().remove(source);
				events.clear();
				this.writeHistory();
				return;
			}
			else if (method == VoicesEditorEvent.METHOD_MOVE)
			{
				if (mode == MODE_MOVING && source == preview)
				{
					VoicesTextureObject o = preview.makeCopy();
					preview = null;
					o.setMode(VoicesTextureObject.MODE_IN_EDITOR);
					level.addTextureObject(o);
					
					VoicesTextureObjectTemplate vTemp = VoicesGameBoard.getInstance().getTextureObjectTemplate();
					if (vTemp != null)
					{
						preview = VoicesTextureObjectFactory.createVoicesTextureObject(vTemp, new Vector2f(0, 0));
						preview.setListener(this);
						preview.setMode(VoicesObstacle.MODE_PREVIEW);
					}
					else
					{
						level.getTextureObjects().remove(source);
						preview = null;
					}
					
					setMode(MODE_NORMAL);
					events.clear();
					this.writeHistory();
					return;
				}
				else if (mode == MODE_NORMAL && source != preview && !moveModeCooldown)
				{
					level.getTextureObjects().remove(source);
					preview = source.makeCopy();
					preview.setListener(this);
					preview.setMode(VoicesTextureObject.MODE_PREVIEW);
					
					setMode(MODE_MOVING);
					events.clear();
					return;
				}
			}
		}
	}
	
	protected void updatePlayerEvents(Stack<VoicesPlayerEvent> s)
	{
		
	}
	
	private void writeHistory()
	{
		//Must be a victor.
		while (historyIndex < levelHistory.size() - 1)
		{
			levelHistory.removeLast();
		}
		levelHistory.add(new VoicesLevelTemplate(level));
		historyIndex = levelHistory.size() - 1;
	}
	
	private void undo()
	{
		if(historyIndex > 0)
		{
			historyIndex--;
			loadLevelFromHistory();
		}
	}
	
	private void redo()
	{
		if(historyIndex < levelHistory.size() - 1)
		{
			historyIndex++;
			loadLevelFromHistory();
		}		
	}
	
	private void loadLevelFromHistory()
	{
		try
		{
			VoicesGameBoard.getInstance().setLevelTemplate(levelHistory.get(historyIndex));
			level = new VoicesLevel(VoicesGameBoard.getInstance().getLevelTemplate(), this);
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		
		for(VoicesTextureObject o : level.getTextureObjects())
		{
			o.setMode(VoicesTextureObject.MODE_IN_EDITOR);
		}
	}
	
	public void enter(GameContainer gc, StateBasedGame sbg)
	{
		super.enter(gc, sbg);
		setMode(MODE_NORMAL);
		
		VoicesGameBoard.getInstance().loadLevelTemplates();
		try
		{
			level = new VoicesLevel(VoicesGameBoard.getInstance().getLevelTemplate(), this);
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		
		levelHistory.clear();
		levelHistory.add(new VoicesLevelTemplate(level));
		historyIndex = 0;
		
		VoicesTextureObjectTemplate vTemp = VoicesGameBoard.getInstance().getTextureObjectTemplate();
		if (vTemp != null)
		{
			preview = VoicesTextureObjectFactory.createVoicesTextureObject(vTemp, new Vector2f(0, 0));
			preview.setListener(this);
			
			try
			{
				preview.rescaleVirtualVectors();
			}
			catch (SlickException e)
			{
				e.printStackTrace();
			}
			
			preview.setMode(VoicesTextureObject.MODE_PREVIEW);
		}
		else
		{
			preview = null;
		}
		
		for(VoicesTextureObject o : level.getTextureObjects())
		{
			o.setMode(VoicesTextureObject.MODE_IN_EDITOR);
		}
	}
	
	public void mouseReleased(int button, int x, int y) 
	{
		super.mouseReleased(button, x, y);
		if(preview != null)
		{
			preview.mouseReleased(button, x, y);
		}
	}
	public void mouseMoved(int oldx, int oldy, int x, int y) 
	{
		super.mouseMoved(oldx, oldy, x, y);
		if(preview != null)
		{
			preview.mouseMoved(oldx, oldy, x, y);
			preview.snapToTextureObjects(level.getTextureObjects(), new Vector2f(0,0));
		}
	}
}
