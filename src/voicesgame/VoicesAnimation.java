package voicesgame;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class VoicesAnimation
{
	private String textureDir;
	
	private boolean looping;
	public boolean isLooping() { return looping; }
	public void setLooping(boolean isLooping) { looping = isLooping; }
	
	private boolean done = false;
	public boolean isDone() { return done; }
	
	private float fps;
	
	private LinkedList<Image> textures = new LinkedList<Image>();
	public void setTextures(String fileName)
	{
		if (textureDir != fileName)
		{
			textures.clear();
			done = false;
			index = 0;
			fps = -1;
			textureDir = fileName;
			
			File dir = new File(fileName);
			File[] imageFiles = dir.listFiles();
			Arrays.sort(imageFiles);
			
			for (File f : imageFiles)
			{
				if (f.getName().equals("fps.txt"))
				{
					try
					{
						BufferedReader br = new BufferedReader(new FileReader(f));
						String fpsText = br.readLine();
						fps = Float.parseFloat(fpsText);
						br.close();
					}
					catch (IOException e)
					{
						e.printStackTrace();
					}
				}
				else if (f.getName().contains(".png"))
				{
					try
					{
						Image i = new Image(f.getPath());
						i.setFilter(Image.FILTER_NEAREST);
						textures.add(i);
					}
					catch (SlickException e)
					{
						e.printStackTrace();
					}
				}
			}
			if (fps == -1)
			{
				System.out.println("fps.txt does not exist in the following folder: " + fileName);
				System.exit(60);
			}
		}
	}
	
	private float index = 0;
	public int getIndex(){ return (int)index; }
	
	public Image getTexture() { return textures.get(getIndex()); }
	
	public VoicesAnimation(boolean isLooping)
	{
		looping = isLooping;
	}
	
	public void advance(int delta)
	{
		float seconds = delta/1000f;
		index += seconds * fps;
		
		if (getIndex() >= textures.size())
		{
			if (looping)
			{
				index -= getIndex();
			}
			else
			{
				index = textures.size() - 1;
				done = true;
			}
		}
	}
}
