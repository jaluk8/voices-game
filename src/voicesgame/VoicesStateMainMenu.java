package voicesgame;

import java.util.Stack;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.*;
import org.newdawn.slick.geom.Vector2f;

public class VoicesStateMainMenu extends VoicesState
{
	public VoicesStateMainMenu(int s) throws SlickException
	{
		super(s);
		
		VoicesTextBox titleBox = new VoicesTextBox(new Vector2f(0, 64), new Vector2f(1024, 64), "title",Color.blue, "Voices: Can this even be called alpha?");
		addMenuObject(titleBox);
		
		VoicesButton playButton = new VoicesButton(new Vector2f(256, 192), new Vector2f(512, 64), "play", Color.yellow, "Play now for free!");
		addMenuObject(playButton);
		
		VoicesButton editButton = new VoicesButton(new Vector2f(256, 320), new Vector2f(512, 64), "edit", Color.green, "Level Editor");
		addMenuObject(editButton);
		
		VoicesButton exitButton = new VoicesButton(new Vector2f(256, 448), new Vector2f(512, 64), "exit", Color.red, "Exit");
		addMenuObject(exitButton);
	}
	
	protected void updateMenuEvents(Stack<VoicesMenuEvent> s)
	{
		while (s.size() > 0)
		{
			VoicesMenuEvent e = s.pop();
			VoicesMenuObject source = e.getMenuObject();
			
			try
			{
				rescaleVirtualVectors();
			}
			catch (SlickException e1)
			{
				e1.printStackTrace();
			}
			
			switch (source.getName())
			{
			case "play":
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_PLAY_PICKER);
				break;
			case "edit":
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_PICKER);
				break;
			case "exit":
				VoicesGameBoard.getInstance().game.getContainer().exit();
			default:
				break;
			}
		}
	}

	protected void updatePlayerEvents(Stack<VoicesPlayerEvent> s)
	{
		
	}

	protected void updateEditorEvents(Stack<VoicesEditorEvent> s)
	{
		
	}
	
	public void keyReleased(int key, char c) 
	{
		if (key == Keyboard.KEY_ESCAPE)
		{
			VoicesGameBoard.getInstance().game.getContainer().exit();
		}
	}
}
