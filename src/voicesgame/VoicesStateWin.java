package voicesgame;

import java.util.Stack;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;

public class VoicesStateWin extends VoicesScreenBasedState
{
	public VoicesStateWin(int s)
	{
		super(s);
		this.setBackgroundTextureName("win");
		
		VoicesButton retryButton = new VoicesButton(new Vector2f(0, 512), new Vector2f(510, 64), "retry", Color.green, "Try again for no reason");
		addMenuObject(retryButton);

		VoicesButton exitButton = new VoicesButton(new Vector2f(516, 512), new Vector2f(510, 64), "exit", Color.red, "Exit to main menu");
		addMenuObject(exitButton);
	}

	protected void updateMenuEvents(Stack<VoicesMenuEvent> s)
	{
		for (VoicesMenuEvent e : s)
		{
			switch (e.getMenuObject().getName())
			{
			case "retry":
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_PLAY, VoicesStatePlay.MODE_REGULAR);
				break;
			case "exit":
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_MAIN_MENU);
				break;
			}
		}
	}

	protected void updatePlayerEvents(Stack<VoicesPlayerEvent> s)
	{
		
	}

	protected void updateEditorEvents(Stack<VoicesEditorEvent> s)
	{
		
	}
}
