package voicesgame;

import java.util.LinkedList;
import java.util.Stack;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public abstract class VoicesTemplatePicker extends VoicesState 
{
	protected final int MODE_NORMAL = 0;
	protected final int MODE_SELECTED = 1;
	protected final int MODE_NO_EDIT = 2;
	
	private LinkedList<VoicesTemplate> templates;
	public LinkedList<VoicesTemplate> getTemplates() { return templates; }
	public void setTemplates (LinkedList<VoicesTemplate> aTemp) { templates = aTemp; }
	public void addTemplate (VoicesTemplate t)
	{
		templates.add(t);
	}
	
	protected VoicesTemplate chosenTemplate;
	
	private VoicesButton addButton;
	private VoicesButton chooseButton;
	private VoicesButton editButton;
	private VoicesButton deleteButton;
	
	public VoicesTemplatePicker(int s) 
	{
		super(s);
		addButton    = new VoicesButton(new Vector2f(0, 0),   new Vector2f(1024, 64), "add",    Color.green, "Add Template");
		chooseButton = new VoicesButton(new Vector2f(0, 0),   new Vector2f(333, 64), "choose", Color.yellow, "Choose Template");
		editButton = new VoicesButton(new Vector2f(341, 0),   new Vector2f(333, 64), "choose", Color.orange, "Edit Template");
		deleteButton = new VoicesButton(new Vector2f(682, 0), new Vector2f(333, 64), "delete", Color.red, "Delete Template");
	
		chooseButton.setVisibility(false);
		editButton.setVisibility(false);
		deleteButton.setVisibility(false);
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException 
	{
		super.render(gc, sbg, g);
	}
	
	protected void updateMenuEvents(Stack<VoicesMenuEvent> s)
	{
		while (s.size() > 0)
		{
			VoicesMenuEvent e = s.pop();
			VoicesMenuObject source = e.getMenuObject();
			VoicesTemplate t = findTemplateFromId(source.getName());
			
			if (source == addButton)
			{
				addTemplate();
			}
			else if (source == chooseButton)
			{
				chooseTemplate();
			}
			else if (source == editButton)
			{
				editTemplate();
			}
			else if (source == deleteButton)
			{
				removeTemplate();
			}
			else if (t != null)
			{
				if (getMode() == MODE_NO_EDIT)
				{
					chosenTemplate = t;
					source.setSelected(true);
					chooseTemplate();
				}
				else if (getMode() == MODE_NORMAL)
				{
					for (VoicesMenuObject o : menuObjects)
					{
						o.setSelected(false);
					}
					chosenTemplate = t;
					source.setSelected(true);
					setMode(MODE_SELECTED);
				}
				else
				{
					chosenTemplate = null;
					source.setSelected(false);
					setMode(MODE_NORMAL);
				}
			}
		}
	}

	protected void updatePlayerEvents(Stack<VoicesPlayerEvent> s)
	{
		
	}

	protected void updateEditorEvents(Stack<VoicesEditorEvent> s)
	{
		
	}
	
	public VoicesTemplate findTemplateFromId(String anId)
	{
		int i;
		try { i = Integer.parseInt(anId); }
		catch (NumberFormatException e)
		{
			return null;
		}
		for (VoicesTemplate t : templates)
		{
			if (t.getId() == i)
			{
				return t;
			}
		}
		return null;
	}
	
	public void setMode(int aMode)
	{
		switch (aMode)
		{
		case MODE_NORMAL:
			addButton.setVisibility(true);
			chooseButton.setVisibility(false);
			editButton.setVisibility(false);
			deleteButton.setVisibility(false);
			chosenTemplate = null;
			break;
		case MODE_SELECTED:
			addButton.setVisibility(false);
			chooseButton.setVisibility(true);
			editButton.setVisibility(true);
			deleteButton.setVisibility(true);
			break;
		case MODE_NO_EDIT:
			addButton.setVisibility(false);
			chooseButton.setVisibility(false);
			editButton.setVisibility(false);
			deleteButton.setVisibility(false);
			break;
		}
		mode = aMode;
	}
	
	public void createButtons()
	{
		menuObjects = new LinkedList<VoicesMenuObject>();
		for (int i=0; i<templates.size(); i++)
		{
			int yOffset = 68;
			if (mode == MODE_NO_EDIT) { yOffset = 0; }
			
			VoicesTemplate t = templates.get(i);
			Vector2f pos = new Vector2f(0, i*68 + yOffset);
			Vector2f size = new Vector2f(1024, 64);
			VoicesButton b = new VoicesButton(pos, size, Integer.toString(t.getId()), Color.blue, t.getName());
			addMenuObject(b);
		}
		
		addMenuObject(addButton);
		addMenuObject(chooseButton);
		addMenuObject(editButton);
		addMenuObject(deleteButton);

		for (VoicesMenuObject o : menuObjects)
		{
			try
			{
				o.rescaleVirtualVectors();
			}
			catch(SlickException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public abstract void enter(GameContainer gc, StateBasedGame sbg);
	public abstract void chooseTemplate();
	public abstract void editTemplate();
	public abstract void removeTemplate();
	public abstract void addTemplate();
}
