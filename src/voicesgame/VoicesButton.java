package voicesgame;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesButton extends VoicesMenuObject
{	
	static final int MODE_NORMAL = 0;
	static final int MODE_HOVERED = 1;
	static final int MODE_PRESSED = 2;
	
	public VoicesButton(Vector2f aPos, Vector2f aSize, String aName, Color aTextColor, String aContent)
	{
		super(aPos, aSize, "res/menu/buttons", aName, aTextColor, aContent);
	}
	
	public VoicesTextureObject makeCopy()
	{
		VoicesButton b2 = new VoicesButton(position.getPixel().copy(), size.getPixel().copy(), getName(), getTextColor(), getContent());
		return b2;
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g, boolean scroll) throws SlickException
	{
		if (getVisibility())
		{
			setSelected(getMode() != MODE_NORMAL);
			super.render(gc, sbg, g, scroll);
		}
	}
	
	public void mouseMoved(int oldx, int oldy, int x, int y) 
	{
		Vector2f pos = new Vector2f(x, y);
		boolean hit = isHit(pos, false);
		if (hit)
		{
			if (getMode() == MODE_NORMAL) { setMode(MODE_HOVERED); }
		}
		else
		{
			if (getMode() == MODE_HOVERED) { setMode(MODE_NORMAL); }
		}
	}
	
	public void mousePressed(int button, int x, int y) 
	{
		if (getVisibility())
		{
			Vector2f pos = new Vector2f(x, y);
			if (isHit(pos, false))
			{
				setMode(MODE_PRESSED);
			}
		}
	}
	
	public void mouseReleased(int button, int x, int y) 
	{
		Vector2f pos = new Vector2f(x, y);
		if (isHit(pos, false))
		{
			if (getMode() == MODE_PRESSED) 
			{
				listener.addMenuEvent(this, button);
				setMode(MODE_HOVERED);
			}
		}
		else if (getMode() != MODE_NORMAL) 
		{
			setMode(MODE_NORMAL);
		}
	}
	
	public void loadTextureFromMode()
	{
		String filePath = getTextureFolderName();
		switch(getMode())
		{
		case MODE_NORMAL:
			filePath += "/normal";
			break;
		case MODE_HOVERED:
			filePath += "/hovered";
			break;
		case MODE_PRESSED:
			filePath += "/pressed";
			break;
		}
		setSourceAnimation(filePath);
	}
}
