package voicesgame;

import java.util.LinkedList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public abstract class VoicesTextureObject
{
	static final int MODE_NORMAL = 0;
	static final int MODE_IN_EDITOR = -1;
	static final int MODE_PREVIEW = -2;
	static final int MODE_ADDER = -3;
	
	private int mode = 0; 
	public int getMode() { return mode; }
	
	protected Color color = new Color(1f, 1f, 1f);
	public void setColor(Color c) { color = c; }
	
	private boolean translucent = false;
	public boolean isTranslucent() { return translucent; }
	public void setTranslucent(boolean i)
	{
		translucent = i;
		if (i)
		{
			color.a = 0.5f;
		}
		else
		{
			color.a = 1;
		}
	}
	
	protected VoicesState listener;
	public VoicesState getListener() { return listener; }
	public void setListener(VoicesState l) { listener = l; }
	
	public VoicesPositionVector position = new VoicesPositionVector();
	public VoicesSizeVector size = new VoicesSizeVector();
	
	private String templateName;
	public String getTemplateName() { return templateName; }
	
	private int templateId;
	public int getTemplateId() { return templateId; }
	
	protected VoicesAnimation sourceAnimation = new VoicesAnimation(true);
	public VoicesAnimation getSourceAnimation() { return sourceAnimation; }
	public void setSourceAnimation(String aFile) 
	{ 
		sourceAnimation.setTextures(aFile);
	}
	
	protected String textureFolderName;
	public String getTextureFolderName() { return textureFolderName; }
	
	public VoicesTextureObject(Vector2f aPos, Vector2f aSize, String aTextureName) 
	{
		position.setPixel(aPos);
		if (aSize != null)
		{
			size.setPixel(aSize);
		}
		textureFolderName = aTextureName;
	}
	public VoicesTextureObject(Vector2f aPos, Vector2f aSize, String aTextureName, String aTempName, int aTempId) 
	{
		position.setPixel(aPos);
		if (aSize != null)
		{
			size.setPixel(aSize);
		}
		textureFolderName = aTextureName;
		templateName = aTempName;
		templateId = aTempId;
	}
	public abstract VoicesTextureObject makeCopy();
	
	public boolean isHit(Vector2f pos, boolean scrolling)
	{
		float sd = VoicesGameBoard.getInstance().SNAP_DISTANCE;
		
		Vector2f aPos = position.getVirtual();
		if (scrolling)
		{
			aPos = position.getScrolled();
		}
		
		boolean xhit = (aPos.x <= pos.x + sd) && (aPos.x + size.getVirtual().x >= pos.x - sd);
		boolean yhit = (aPos.y <= pos.y + sd) && (aPos.y + size.getVirtual().y >= pos.y - sd);
		return xhit && yhit;
	}
	
	public void setMode(int aMode)
	{
		mode = aMode;
		
		try
		{
			rescaleVirtualVectors();
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		
		if (aMode == MODE_PREVIEW)
		{
			position.setScrolled(VoicesGameBoard.getMousePosition().getVirtual());
			position.snapToGrid();
		}
	}
	
	public void resetMode()
	{
		setMode(getMode());
	}
	
	public void rescaleVirtualVectors() throws SlickException
	{
		loadTextureFromMode();
		if (size.isNull)
		{
			size.setPixel(new Vector2f(sourceAnimation.getTexture().getHeight(), sourceAnimation.getTexture().getWidth()));
		}
		size.rescaleVirtuals();
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
	{
		rescaleVirtualVectors();
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g, boolean scroll) throws SlickException
	{
		Vector2f virtualPos = position.getVirtual();
		Vector2f virtualSize = size.getVirtual();
		
		if (scroll)
		{
			virtualPos = position.getScrolled();
		}
		
		float x_min = -virtualSize.x;
		float x_max = VoicesGameBoard.getInstance().scaleScalar(VoicesGameBoard.getInstance().PIXELS_PER_SCREEN_WIDTH, true);
		float y_min = -virtualSize.y;
		float y_max = VoicesGameBoard.getInstance().scaleScalar(VoicesGameBoard.getInstance().PIXELS_PER_SCREEN_HEIGHT, false);
		if (virtualPos.x < x_max && virtualPos.y < y_max && virtualPos.x > x_min && virtualPos.y > y_min)
		{
			g.drawImage(sourceAnimation.getTexture(), virtualPos.x, virtualPos.y, virtualPos.x + virtualSize.x, virtualPos.y + virtualSize.y, 0, 0, size.getPixel().x, size.getPixel().y, color);
		}
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException 
	{
		sourceAnimation.advance(delta);
	}
	
	public void snapToTextureObjects(LinkedList<VoicesTextureObject> textureObjects, Vector2f offset)
	{
		position.snapToTextureObjects(textureObjects, new Vector2f(0, 0).add(offset));
		position.snapToTextureObjects(textureObjects, new Vector2f(size.getPixel().x, 0).add(offset));
		position.snapToTextureObjects(textureObjects, new Vector2f(0, size.getPixel().y).add(offset));
		position.snapToTextureObjects(textureObjects, new Vector2f(size.getPixel().x, size.getPixel().y).add(offset));
	}
	
	abstract public void loadTextureFromMode();
	
	public void mousePressed(int button, int x, int y) { };
	public void mouseReleased(int button, int x, int y) { }; 
	public void mouseMoved(int oldx, int oldy, int x, int y) { };
	public void keyPressed(int key, char c) throws SlickException { };
	public void keyReleased(int key, char c) { };
}
