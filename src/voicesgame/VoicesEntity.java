package voicesgame;

import java.util.LinkedList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public abstract class VoicesEntity extends VoicesTextureObject 
{
	public final double gravity = 0.001;
	
	static final int MODE_DEATH = -666;
	
	public static final int TEAM_PLAYER = 0;
	public static final int TEAM_ENEMY = 1;
	public static final int TEAM_NO_ONE = 2;
	
	public VoicesPositionVector oldPosition = new VoicesPositionVector();
	
	public boolean onTheFloor = false;
	public boolean onTheCeil = false;
	public boolean onTheRight = false;
	public boolean onTheLeft = false;
	public boolean boosting = false;
	
	protected Vector2f velocity;
	public Vector2f getVelocity() { return velocity; }
	public void setVelocity(Vector2f aVel) { velocity = aVel; }
	
	protected int team;
	public int getTeam() { return team; }
	public void setTeam(int t) { team = t; }
	
	private boolean hasGravity = true;
	public boolean getGravity() { return hasGravity; }
	public void setGravity(boolean g) { hasGravity = g; }
	
	private boolean collideable = true;
	public boolean canCollide() { return collideable; }
	public void setCollide(boolean c) { collideable = c; }
		
	protected LinkedList<VoicesWhisp> whisps = new LinkedList<VoicesWhisp>();
	public LinkedList<VoicesWhisp> getWhisps() { return whisps; }
	public void setWhisps(LinkedList<VoicesWhisp> someWhisps)
	{
		whisps = someWhisps;
	}
	public void addWhisp(VoicesWhisp w)
	{
		whisps.add(w);
	}
		
	public VoicesEntity(Vector2f aPos, String aTexture, int anId, int aTeam)
	{
		super(aPos, null, aTexture, "Unknown Entity", anId);
		velocity = new Vector2f(0, 0);
		setTeam(aTeam);
	}
	
	public VoicesWhisp getFirstWhispOfType(Class<?> type, boolean allowUnavailable)
	{
		for (VoicesWhisp w : whisps)
		{
			boolean available = w.getMode() == VoicesWhisp.MODE_FOLLOWING;
			if (w.getClass() == type && (allowUnavailable || available))
			{
				return w;
			}
		}
		return null;
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException 
	{
		if (oldPosition.isNull())
		{
			oldPosition.setPixel(position.getPixel().copy());
		}
		
		if (getMode() != MODE_IN_EDITOR && getMode() != MODE_PREVIEW)
		{
			if(hasGravity && !onTheFloor)
			{
				velocity.y += gravity * delta;
			}
			
			Vector2f pixelVelocity = velocity.copy().scale(delta);
			oldPosition.setPixel(position.getPixel().copy());
			position.setPixel(position.getPixel().add(pixelVelocity));
			
			if (collideable)
			{
				collide();
			}
		}
		super.update(gc, sbg, delta);
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g, boolean scroll) throws SlickException
	{
		super.render(gc, sbg, g, scroll);
	}
	
	private void collide()
	{
		Vector2f posDelta = oldPosition.getPixel().copy().add(position.getPixel().copy().scale(-1));
	
		float posX = position.getPixel().x;
		float posY = position.getPixel().y;
		float sizeX = size.getPixel().x;
		float sizeY = size.getPixel().y;
		
		Vector2f cornerTR = new Vector2f(posX, posY);
		Vector2f cornerTL = new Vector2f(posX + sizeX, posY);
		Vector2f cornerBR = new Vector2f(posX, posY + sizeY);
		Vector2f cornerBL = new Vector2f(posX+sizeX, posY + sizeY);
		
		onTheFloor = false;
		onTheCeil = false;
		onTheRight = false;
		onTheLeft = false;
		for (VoicesTextureObject vto : VoicesGameBoard.getCurrentLevel().getTextureObjects())
		{
			if (vto instanceof VoicesObstacle && !vto.isTranslucent())
			{
				VoicesObstacle o = (VoicesObstacle)vto;
				for (VoicesWall w : o.getWalls())
				{
					boolean floorOrLeft = w.delta().x >= w.delta().y;
					boolean floorOrCeil = Math.abs(w.delta().x) >= Math.abs(w.delta().y);
					
					if (floorOrLeft)
					{
						if(floorOrCeil)
						{
							collideFloor(new Vector2f[]{cornerBR, cornerBL}, w, posDelta, o.position.getPixel());
						}
						else
						{
							collideLeft(new Vector2f[]{cornerBL, cornerTL}, w, posDelta, o.position.getPixel());
						}
					}
					else
					{
						if(floorOrCeil)
						{
							collideCeil(new Vector2f[]{cornerTR, cornerTL}, w, posDelta, o.position.getPixel());
						}
						else
						{
							collideRight(new Vector2f[]{cornerBR, cornerTR}, w, posDelta, o.position.getPixel());
						}
					}
				}
			}
		}
		
		resetMode();
		
		if (onTheFloor || onTheCeil)
		{
			velocity.y = 0;
			boosting = false;
		}
		else if (onTheRight || onTheLeft)
		{
			velocity.x = 0;
			boosting = false;
		}
	}
	
	protected void collideFloor(Vector2f[] corners, VoicesWall w, Vector2f posDelta, Vector2f offset)
	{
		float yOffset = 0;
		for (Vector2f c : corners)
		{
			boolean collision = w.collidedWithPosition(c, c.copy().add(posDelta), offset, true);
			if (collision)
			{
				if (velocity.y >= 0)
				{
					onTheFloor = true;
				}
				
				float newY = (c.x - w.position1.getPixel().x - offset.x) * (w.delta().y / w.delta().x) + w.position1.getPixel().y + offset.y;
				if (newY < c.y + yOffset)
				{
					yOffset = newY - c.y;
				}
			}
		}
		position.getPixel().y += yOffset;
	}
	
	protected void collideCeil(Vector2f[] corners, VoicesWall w, Vector2f posDelta, Vector2f offset)
	{
		float yOffset = 0;
		for (Vector2f c : corners)
		{
			boolean collision = w.collidedWithPosition(c, c.copy().add(posDelta), offset, true);
			if (collision)
			{
				if (velocity.y <= 0)
				{
					onTheCeil = true;
				}
				
				float newY = (c.x - w.position1.getPixel().x - offset.x) * (w.delta().y / w.delta().x) + w.position1.getPixel().y + offset.y;
				if (newY > c.y + yOffset)
				{
					yOffset = newY - c.y;
				}
			}
		}
		position.getPixel().y += yOffset;
	}
	
	protected void collideLeft(Vector2f[] corners, VoicesWall w, Vector2f posDelta, Vector2f offset)
	{
		float xOffset = 0;
		for (Vector2f c : corners)
		{
			boolean collision = w.collidedWithPosition(c, c.copy().add(posDelta), offset, false);
			if (collision)
			{
				if (velocity.x <= 0)
				{
					onTheLeft = true;
				}
				
				float newX = (c.y - w.position1.getPixel().y - offset.y) * (w.delta().x / w.delta().y) + w.position1.getPixel().x + offset.x;
				if (newX < c.x + xOffset)
				{
					xOffset = newX - c.x;
				}
			}
		}
		position.getPixel().x += xOffset;
	}
	
	protected void collideRight(Vector2f[] corners, VoicesWall w, Vector2f posDelta, Vector2f offset)
	{
		float xOffset = 0;
		for (Vector2f c : corners)
		{
			boolean collision = w.collidedWithPosition(c, c.copy().add(posDelta), offset, false);
			if (collision)
			{
				if (velocity.x >= 0)
				{
					onTheRight = true;
				}
				
				float newX = (c.y - w.position1.getPixel().y - offset.y) * (w.delta().x / w.delta().y) + w.position1.getPixel().x + offset.x;
				if (newX > c.x + xOffset)
				{
					xOffset = newX - c.x;
				}
			}
		}
		position.getPixel().x += xOffset;
	}
	
	public abstract void hit();
	
	public void notifyOfAttack(VoicesWhispAttack attacker)
	{
		VoicesWhispHealth lastChance = (VoicesWhispHealth) this.getFirstWhispOfType(VoicesWhispHealth.class, false);
		
		if (lastChance != null)
		{
			lastChance.chaseVTO(attacker);
			attacker.attack(lastChance);
		}
	}
	
	public void mouseReleased(int button, int x, int y)
	{
		if (isHit(new Vector2f(x, y), true))
		{
			listener.addEditorEvent((VoicesEntity)this, button);
		}
	}
	public void mouseMoved(int oldx, int oldy, int x, int y)
	{
		if (getMode() == MODE_PREVIEW)
		{
			position.setScrolled(new Vector2f(x, y));
			position.snapToGrid();
		}
	}
}
