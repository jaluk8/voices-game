package voicesgame;

import org.newdawn.slick.geom.Vector2f;

public class VoicesTextureObjectFactory
{	
	public static VoicesTextureObject createVoicesTextureObject(VoicesTextureObjectTemplate aTemp, Vector2f pos)
	{
		VoicesTextureObject vto = null;
		
		if (aTemp.getType().equals(VoicesObstacle.class.getName()))
		{
			vto = new VoicesObstacle(pos, aTemp.getSize(), aTemp.getTextureFolderName(), aTemp.getName(), aTemp.getId(), aTemp.getWalls(), aTemp.isTranslucent());
		}
		else if (aTemp.getType().equals(VoicesWhispHealth.class.getName()))
		{
			vto = new VoicesWhispHealth(pos, aTemp.getId());
		}
		else if (aTemp.getType().equals(VoicesWhispBoost.class.getName()))
		{
			vto = new VoicesWhispBoost(pos, aTemp.getId());
		}
		else if (aTemp.getType().equals(VoicesWhispBridge.class.getName()))
		{
			vto = new VoicesWhispBridge(pos, aTemp.getId());
		}
		else if (aTemp.getType().equals(VoicesWhispAttack.class.getName()))
		{
			vto = new VoicesWhispAttack(pos, aTemp.getId());
		}
		else
		{
			System.out.println("Invalid type of VoicesTextureObjectTemplate: " + aTemp.getType());
		}
		return vto;
	}
}
