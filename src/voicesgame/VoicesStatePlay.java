package voicesgame;

import java.util.LinkedList;
import java.util.Stack;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesStatePlay extends VoicesLevelBasedState
{
	public static final int MODE_REGULAR = 0;
	public static final int MODE_TESTING = 1;
	
	public final float SCROLL_SPEED = VoicesPlayer.MAX_WALK_SPEED;
	
	private Vector2f scrollLimit = null;
	public void setScrollLimit()
	{
		float maxX = VoicesGameBoard.getInstance().PIXELS_PER_SCREEN_WIDTH;
		float minY = 0;
		
		for (VoicesTextureObject vto: level.getTextureObjects())
		{
			Vector2f pos = vto.position.getPixel();
			Vector2f size = vto.size.getPixel();
			if (pos.x +size.x > maxX)
			{
				maxX = pos.x + size.x;
			}
			if (pos.y< minY)
			{
				minY = pos.y;
			}
		}

		scrollLimit = new Vector2f(maxX, minY);
	}
	
	private boolean paused = false;
	public boolean isPaused() { return paused; }
	public void setPaused(boolean p)
	{
		paused = p;
		updatePauseButtons();
	}
	
	public void togglePaused()
	{
		if (paused)
		{
			setPaused(false);
		}
		else
		{
			setPaused(true);
		}
	}
	
	private LinkedList<VoicesWhisp> hudWhisps = new LinkedList<VoicesWhisp>();
	public LinkedList<VoicesWhisp> getHudWhisps() { return hudWhisps; }
	public void updateHudWhisps()
	{
		hudWhisps.clear();
		LinkedList<VoicesWhisp> playerWhisps = this.level.getPlayer().getWhisps();
		float x = 0;
		
		for(VoicesWhisp whisp : playerWhisps)
		{
			if (whisp.getMode() == VoicesWhisp.MODE_FOLLOWING)
			{
				VoicesWhisp w = (VoicesWhisp) whisp.makeCopy();
				w.setMode(VoicesWhisp.MODE_IN_EDITOR);
				w.position.setPixel(new Vector2f(x, 0));
				hudWhisps.add(w);
				
				x += w.size.getPixel().x;
			}
		}
	}
	
	private VoicesButton pauseUnpause;
	private VoicesButton pauseRetry;
	private VoicesButton pauseExit;
	
	public VoicesStatePlay(int s)
	{
		super(s, VoicesGame.STATE_LEVEL_PLAY_PICKER);
		
		pauseUnpause = new VoicesButton(new Vector2f(0, 0), new Vector2f(339, 64), "pauseUnpause", Color.yellow, "Unpause");
		addMenuObject(pauseUnpause);
		
		pauseRetry = new VoicesButton(new Vector2f(343, 0), new Vector2f(338, 64), "pauseRetry", Color.green, "Start over");
		addMenuObject(pauseRetry);
		
		pauseExit = new VoicesButton(new Vector2f(685, 0), new Vector2f(339, 64), "pauseExit", Color.red, "Exit");
		addMenuObject(pauseExit);
		
		setMode(MODE_REGULAR);
	}

	public void setMode(int aMode)
	{
		super.setMode(aMode);
		updatePauseButtons();
	}
	
	private void updatePauseButtons()
	{
		if(paused)
		{
			pauseUnpause.setVisibility(true);
			pauseRetry.setVisibility(true);
			pauseExit.setVisibility(true);
		}
		else
		{
			pauseUnpause.setVisibility(false);
			pauseRetry.setVisibility(false);
			pauseExit.setVisibility(false);
		}
	}
	
	private void updateCamera(int delta)
	{
		if (scrollLimit == null)
		{
			setScrollLimit();
		}
		Vector2f scroll = VoicesGameBoard.getInstance().getScroll().copy();
		
		float playerX = level.getPlayer().position.getPixel().x + level.getPlayer().size.getPixel().x / 2;
		float playerY = level.getPlayer().position.getPixel().y + level.getPlayer().size.getPixel().y / 2;
		float intendedX = VoicesGameBoard.getInstance().PIXELS_PER_SCREEN_WIDTH/2 - playerX;
		float intendedY = -VoicesGameBoard.getInstance().PIXELS_PER_SCREEN_HEIGHT/2 + playerY;
		
		if (intendedX < scroll.x)
		{
			scroll.x -= SCROLL_SPEED * delta;
			if (intendedX > scroll.x)
			{
				scroll.x = intendedX;
			}
		}
		else if (intendedX > scroll.x)
		{
			scroll.x += SCROLL_SPEED * delta;
			if (intendedX < scroll.x)
			{
				scroll.x = intendedX;
			}
		}
		if (intendedY < scroll.y)
		{
			scroll.y -= SCROLL_SPEED * delta;
			if (intendedY > scroll.y)
			{
				scroll.y = intendedY;
			}
		}
		else if (intendedY > scroll.y)
		{
			scroll.y += SCROLL_SPEED * delta;
			if (intendedY < scroll.y)
			{
				scroll.y = intendedY;
			}
		}
		
		float maxX = 0;
		float maxY = 0;
		float minX = VoicesGameBoard.getInstance().PIXELS_PER_SCREEN_WIDTH - scrollLimit.x;
		float minY = scrollLimit.y;	
		
		if (scroll.x > maxX)
		{
			scroll.x = maxX;
		}
		if (scroll.y > maxY)
		{
			scroll.y = maxY;
		}
		if (scroll.x < minX)
		{
			scroll.x = minX;
		}
		if (scroll.y < minY)
		{
			scroll.y = minY;
		}
		
		VoicesGameBoard.getInstance().setScroll(scroll);
	}
	
	public void handleMenuEvent(VoicesMenuEvent e) throws SlickException 
	{
		VoicesMenuObject source = e.getMenuObject();
		if (source == pauseUnpause)
		{
			setPaused(false);
		}
		else if (source == pauseRetry)
		{
			VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_PLAY, getMode());
		}
		else if (source == pauseExit)
		{
			if (getMode() == MODE_TESTING)
			{
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_EDITOR);
			}
			else
			{
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_MAIN_MENU);
			}
		}
	}
	
	protected void updateMenuEvents(Stack<VoicesMenuEvent> s)
	{
		while (s.size() > 0)
		{
			VoicesMenuEvent e = s.pop();
			VoicesMenuObject source = e.getMenuObject();
			
			if (source == pauseUnpause)
			{
				setPaused(false);
			}
			else if (source == pauseRetry)
			{
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_PLAY, getMode());
			}
			else if (source == pauseExit)
			{
				if (getMode() == MODE_TESTING)
				{
					VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_EDITOR);
				}
				else
				{
					VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_MAIN_MENU);
				}
			}
		}
	}

	protected void updatePlayerEvents(Stack<VoicesPlayerEvent> s)
	{
		while (s.size() > 0)
		{
			VoicesPlayerEvent e = s.pop();
			if (e.getEvent() == VoicesPlayerEvent.EVENT_DEATH)
			{
				this.lose();
			}
			else if (e.getEvent() == VoicesPlayerEvent.EVENT_WIN)
			{
				this.win();
			}
		}
	}

	protected void updateEditorEvents(Stack<VoicesEditorEvent> s)
	{
		
	}
	
	public void lose()
	{
		if (getMode() == MODE_TESTING)
		{
			VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_EDITOR);
		}
		else
		{
			VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LOSE);
		}
	}
	
	public void win()
	{
		if (getMode() == MODE_TESTING)
		{
			VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_EDITOR);
		}
		else
		{
			VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_WIN);
		}
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException
	{
		super.render(gc, sbg, g);
		if (!paused)
		{
			for (VoicesWhisp w : hudWhisps)
			{
				w.render(gc, sbg, g, false);
			}
		}
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException
	{
		updateCamera(delta);
		if (!paused)
		{
			level.getPlayer().moveToLevelConstraints(scrollLimit);
		}
		
		if (level != null)
		{
			if (!paused)
			{
				level.update(gc, sbg, delta);
			}
			for (VoicesMenuObject o : menuObjects)
			{
				o.update(gc, sbg, delta);
			}
			
			if (VoicesGameBoard.getInstance().getScroll().x == VoicesGameBoard.getInstance().PIXELS_PER_SCREEN_WIDTH - scrollLimit.x)
			{
				level.getPlayer().ableToWin();
			}
		}
		updateEvents();
	}
	
	public void enter(GameContainer gc, StateBasedGame sbg)
	{
		super.enter(gc, sbg);
		updateHudWhisps();
		VoicesGameBoard.getInstance().setScroll(new Vector2f(0, 0));
		
		VoicesGameBoard.getInstance().loadLevelTemplates();
		try
		{
			level = new VoicesLevel(VoicesGameBoard.getInstance().getLevelTemplate(), this);
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		
		setPaused(false);
		setScrollLimit();
	}
	
	public void keyReleased(int key, char c) 
	{
		if (!paused)
		{
			super.keyReleased(key, c);
		}
		if (key == Keyboard.KEY_ESCAPE)
		{
			togglePaused();
		}
	}
	
	public void keyPressed(int key, char c) 
	{
		if (!paused)
		{
			super.keyPressed(key, c);
		}
	}
}
