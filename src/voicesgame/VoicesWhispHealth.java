package voicesgame;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;

public class VoicesWhispHealth extends VoicesWhisp
{
	public VoicesWhispHealth(Vector2f aPos, int anId, int aTeam)
	{
		super(aPos, Color.red, anId, aTeam);
	}
	public VoicesWhispHealth(Vector2f aPos, int anId)
	{
		super(aPos, Color.red, anId);
	}

	public VoicesTextureObject makeCopy()
	{
		return new VoicesWhispHealth(position.getPixel().copy(), getTemplateId(), team);
	}

}
