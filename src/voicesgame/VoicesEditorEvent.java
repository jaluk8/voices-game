package voicesgame;

public class VoicesEditorEvent extends VoicesEvent
{
	static final int METHOD_ADD = 0;
	static final int METHOD_MOVE = 1;
	static final int METHOD_DELETE = 2;
	
	private VoicesTextureObject object;
	public VoicesTextureObject getTextureObject(){ return object; }
	
	public VoicesEditorEvent(VoicesTextureObject o, int m)
	{
		super(m);
		object = o;
	}
}
