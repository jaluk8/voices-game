package voicesgame;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesWhispBridge extends VoicesWhisp
{
	public VoicesWhispBridge(Vector2f aPos, int anId, int aTeam)
	{
		super(aPos, Color.yellow, anId, aTeam);
	}
	public VoicesWhispBridge(Vector2f aPos, int anId)
	{
		super(aPos, Color.yellow, anId);
	}

	public VoicesTextureObject makeCopy()
	{
		return new VoicesWhispBridge(position.getPixel().copy(), getTemplateId(), team);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException 
	{
		super.update(gc, sbg, delta);
		
		if (getMode() == MODE_CHASING)
		{
			//I realize that I could make this one if statement. However, the collidesWith function is pretty resource intensive and I don't want it to always run. I doubt there is an actual performance difference, but every little BIT counts.
			if (collidesWith(target))
			{
				target.setTranslucent(false);
				this.setMode(MODE_DEATH);
			}
		}
		else if (getMode() == MODE_CHASING)
		{
			if (!target.isTranslucent())
			{
				this.returnToOwner();
			}
		}
	}
	
	public void bridge (VoicesObstacle o)
	{
		chaseVTO(o);
	}
}
