package voicesgame;

import java.util.LinkedList;
import java.util.Stack;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public abstract class VoicesState extends BasicGameState
{
	protected int id;
	public int getID() { return id; }
	
	protected int mode = 0; 
	public int getMode() { return mode; }
	public void setMode(int aMode) { mode = aMode; }
	
	protected LinkedList<VoicesMenuObject> menuObjects = new LinkedList<VoicesMenuObject>();
	public LinkedList<VoicesMenuObject> getMenuObjects() { return menuObjects; }
	@SuppressWarnings("unchecked")
	public LinkedList<VoicesMenuObject> copyMenuObjects()
	{
		return (LinkedList<VoicesMenuObject>)menuObjects.clone();
	}
	public void setMenuObjects(LinkedList<VoicesMenuObject> someObjects) { menuObjects = someObjects; }
	
	protected Stack<VoicesEvent> events = new Stack<VoicesEvent>();
	
	public VoicesState(int s)
	{
		id = s;
	}
	
	public void addMenuObject(VoicesMenuObject o)
	{
		menuObjects.add(o);
		o.setListener(this);
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
	{
		rescaleVirtualVectors();
		LinkedList<VoicesMenuObject> objects = this.copyMenuObjects();
		while (objects.size() > 0)
		{
			VoicesMenuObject o = objects.pop();
			o.init(gc, sbg);
		}
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException
	{
		LinkedList<VoicesMenuObject> objects = this.copyMenuObjects();
		while (objects.size() > 0)
		{
			VoicesMenuObject o = objects.pop();
			o.render(gc, sbg, g, false);
		}
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException
	{
		LinkedList<VoicesMenuObject> objects = this.copyMenuObjects();
		while (objects.size() > 0)
		{
			VoicesMenuObject o = objects.pop();
			o.update(gc, sbg, delta);
		}
		updateEvents();
	}
	
	protected void setTextureObjectActivationListening()
	{
		for (VoicesMenuObject o : menuObjects)
		{
			o.setListener(this);
		}
	}
	
	public void rescaleVirtualVectors() throws SlickException
	{
		VoicesGameBoard.getInstance().setScales();
		LinkedList<VoicesMenuObject> objects = this.copyMenuObjects();
		while (objects.size() > 0)
		{
			VoicesMenuObject o = objects.pop();
			o.rescaleVirtualVectors();
		}
	}
	
	public void mousePressed(int button, int x, int y)
	{
		LinkedList<VoicesMenuObject> objects = this.copyMenuObjects();
		while (objects.size() > 0)
		{
			VoicesMenuObject o = objects.pop();
			o.mousePressed(button, x, y);
		}
	}
	public void mouseReleased(int button, int x, int y) 
	{
		LinkedList<VoicesMenuObject> objects = this.copyMenuObjects();
		while (objects.size() > 0)
		{
			VoicesMenuObject o = objects.pop();
			o.mouseReleased(button, x, y);
		}
	}
	public void mouseMoved(int oldx, int oldy, int x, int y) 
	{
		LinkedList<VoicesMenuObject> objects = this.copyMenuObjects();
		while (objects.size() > 0)
		{
			VoicesMenuObject o = objects.pop();
			o.mouseMoved(oldx, oldy, x, y);
		}
	}
	public void keyPressed(int key, char c) 
	{
		LinkedList<VoicesMenuObject> objects = this.copyMenuObjects();
		while (objects.size() > 0)
		{
			VoicesMenuObject o = objects.pop();
			try 
			{
				o.keyPressed(key, c);
			} 
			catch (SlickException e) 
			{
				e.printStackTrace();
			}
		}
	}
	public void keyReleased(int key, char c) 
	{
		LinkedList<VoicesMenuObject> objects = this.copyMenuObjects();
		while (objects.size() > 0)
		{
			VoicesMenuObject o = objects.pop();
			o.keyReleased(key, c);
		}
	}
	
	public void addPlayerEvent(VoicesPlayer p, int event)
	{
		events.add(new VoicesPlayerEvent(event));
	}
	
	public void addMenuEvent(VoicesMenuObject o, int event)
	{
		events.add(new VoicesMenuEvent(o));
	}
	
	public void addEditorEvent(VoicesTextureObject o, int event)
	{
		events.add(new VoicesEditorEvent(o, event));
	}
	
	protected void updateEvents()
	{
		Stack<VoicesMenuEvent> menuEvents = new Stack<VoicesMenuEvent>();
		Stack<VoicesEditorEvent> editorEvents = new Stack<VoicesEditorEvent>();
		Stack<VoicesPlayerEvent> playerEvents = new Stack<VoicesPlayerEvent>();
		
		for (VoicesEvent e : events)
		{
			if (e instanceof VoicesMenuEvent)
			{
				menuEvents.add((VoicesMenuEvent) e);
			}
			else if (e instanceof VoicesPlayerEvent)
			{
				playerEvents.add((VoicesPlayerEvent) e);
			}
			else if (e instanceof VoicesEditorEvent)
			{
				editorEvents.add((VoicesEditorEvent) e);
			}
		}
		
		updateMenuEvents(menuEvents);
		updatePlayerEvents(playerEvents);
		updateEditorEvents(editorEvents);
		
		events.clear();
	}
	
	protected abstract void updateMenuEvents(Stack<VoicesMenuEvent> s);
	protected abstract void updatePlayerEvents(Stack<VoicesPlayerEvent> s);
	protected abstract void updateEditorEvents(Stack<VoicesEditorEvent> s);
}
