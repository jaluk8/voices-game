package voicesgame;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;

public class VoicesTextBox extends VoicesMenuObject 
{
	public final int MODE_NORMAL = 0;
	
	public VoicesTextBox(Vector2f aPos, Vector2f aSize, String aName, Color aTextColor, String aContent) 
	{
		super(aPos, aSize, "res/menu/textBox", aName, aTextColor, aContent);
	}
	
	public VoicesTextureObject makeCopy()
	{
		VoicesTextBox tb2 = new VoicesTextBox(position.getPixel().copy(), size.getPixel().copy(), getName(), getTextColor(), getContent());
		return tb2;
	}
	
	public void loadTextureFromMode()
	{
		String filePath = getTextureFolderName();
		filePath += "/normal";
		setSourceAnimation(filePath);
	}
}
