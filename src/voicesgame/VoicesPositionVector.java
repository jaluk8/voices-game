package voicesgame;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.LinkedList;

import org.newdawn.slick.geom.Vector2f;

public class VoicesPositionVector implements Serializable
{
	private static final long serialVersionUID = -5043005464473917563L;
	
	private Vector2f pixel;
	public Vector2f getPixel() { return pixel; }
	public void setPixel(Vector2f aPos){ pixel = aPos; }
	
	public Vector2f getVirtual()
	{
		return VoicesGameBoard.getInstance().scaleVector(pixel);
	}
	public void setVirtual(Vector2f aPos)
	{
		pixel = VoicesGameBoard.getInstance().antiScaleVector(aPos);
	}
	
	public Vector2f getScrolled()
	{
		Vector2f aPos = pixel.copy();
		aPos.x += VoicesGameBoard.getInstance().getScroll().x;
		aPos.y -= VoicesGameBoard.getInstance().getScroll().y;
		return VoicesGameBoard.getInstance().scaleVector(aPos);
	}
	public void setScrolled(Vector2f aPos)
	{
		Vector2f newPos = VoicesGameBoard.getInstance().antiScaleVector(aPos);
		newPos.x -= VoicesGameBoard.getInstance().getScroll().x;
		newPos.y += VoicesGameBoard.getInstance().getScroll().y;
		setPixel(newPos);
	}
	
	public VoicesPositionVector()
	{
		
	}
	
	public VoicesPositionVector(Vector2f pos, boolean isVirtual)
	{
		if (isVirtual)
		{
			setVirtual(pos);
		}
		else
		{
			setPixel(pos);
		}
	}
	
	public boolean isNull()
	{
		return pixel == null;
	}
	
	public void snapToGrid()
	{
		int pixelX = (int) pixel.x;
		int pixelY = (int) pixel.y;
		setPixel(new Vector2f(pixelX, pixelY));
	}
	public void snapToTextureObjects(LinkedList<VoicesTextureObject> textureObjects, Vector2f offset)
	{
		if (offset == null)
		{
			offset = new Vector2f(0, 0);
		}
		
		float posX = this.getPixel().x + offset.x;
		float posY = this.getPixel().y + offset.y;
		
		float sd = VoicesGameBoard.getInstance().SNAP_DISTANCE;
		
		for(VoicesTextureObject o : textureObjects)
		{
			float oX1 = o.position.getPixel().x;
			float oY1 = o.position.getPixel().y;
			float oX2 = oX1 + o.size.getPixel().x;
			float oY2 = oY1 + o.size.getPixel().y;
			
			if (posY > oY1 - sd && posY < oY2 + sd)
			{
				if (Math.abs(posX - oX1) < sd)
				{
					posX = oX1;
				}
				else if (Math.abs(posX - oX2) < sd)
				{
					posX = oX2;
				}
			}
			
			if (posX > oX1 - sd && posX < oX2 + sd)
			{
				if (Math.abs(posY - oY1) < sd)
				{
					posY = oY1;
				}
				else if (Math.abs(posY - oY2) < sd)
				{
					posY = oY2;
				}
			}
		}
		
		float newX = posX - offset.x;
		float newY = posY - offset.y;
		setPixel(new Vector2f(newX, newY));
	}
	
	public void snapToWalls(LinkedList<VoicesTextureObject> vtos, Vector2f offset)
	{
		if (offset == null)
		{
			offset = new Vector2f(0, 0);
		}
		
		float posX = this.getPixel().x + offset.x;
		float posY = this.getPixel().y + offset.y;
		
		float sd = VoicesGameBoard.getInstance().SNAP_DISTANCE;
		
		for (VoicesTextureObject vto : vtos)
		{
			if (vto.getClass().getName().equals(VoicesObstacle.class.getName()))
			{
				VoicesObstacle o = (VoicesObstacle) vto;
				Vector2f obstaclePos = o.position.getPixel();
				for (VoicesWall w : o.getWalls())
				{
					if (w.getMode() == VoicesWall.MODE_FULLY_PLACED)
					{
						Vector2f wPos1 = w.position1.getPixel().copy().add(obstaclePos);
						Vector2f wPos2 = w.position2.getPixel().copy().add(obstaclePos);
						if (Math.abs(posX - wPos1.x) < sd && Math.abs(posY - wPos1.y) < sd)
						{
							posX = wPos1.x;
							posY = wPos1.y;
						}
						if (Math.abs(posX - wPos2.x) < sd && Math.abs(posY - wPos2.y) < sd)
						{
							posX = wPos2.x;
							posY = wPos2.y;
						}
					}
				}
			}
		}
		
		float newX = posX - offset.x;
		float newY = posY - offset.y;
		setPixel(new Vector2f(newX, newY));
	}
	
	public void writeObject(ObjectOutputStream out) throws IOException 
	{
		out.writeFloat(getPixel().x);
		out.writeFloat(getPixel().y);
	}

	public void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException 
	{
		Vector2f pos = new Vector2f(in.readFloat(), in.readFloat());
		this.setPixel(pos);
	}
	
	public void readObjectNoData() throws ObjectStreamException 
	{
		//I really need this function?
	}
}
