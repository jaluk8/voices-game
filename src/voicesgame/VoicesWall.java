package voicesgame;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;
import java.util.LinkedList;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesWall implements Serializable
{	
	private static final long serialVersionUID = 3064204120223446915L;

	static final int MODE_NOT_PLACED = 0;
	static final int MODE_HALF_PLACED = 1;
	static final int MODE_FULLY_PLACED = 2;
	
	private int mode = 0; 
	public int getMode() { return mode; }
	
	public VoicesPositionVector position1 = new VoicesPositionVector();
	public VoicesPositionVector position2 = new VoicesPositionVector();
	
	public VoicesWall()
	{
		position1.setPixel(new Vector2f(0, 0));
		position2.setPixel(new Vector2f(0, 0));
	}
	
	public boolean inBounds(VoicesSizeVector size)
	{
		if (position1.getPixel().x < 0 || position1.getPixel().y < 0)
		{
			return false;
		}
		else if (position2.getPixel().x < 0 || position2.getPixel().y < 0)
		{
			return false;
		}
		else if (position1.getPixel().x > size.getPixel().x || position1.getPixel().y > size.getPixel().y)
		{
			return false;
		}
		else if (position2.getPixel().x > size.getPixel().x || position2.getPixel().y > size.getPixel().y)
		{
			return false;
		}
		return true;
	}
	
	public void setMode(int aMode)
	{
		mode = aMode;
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
	{

	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g, Vector2f offset) throws SlickException
	{		
		Vector2f pos1 = position1.getVirtual().copy().add(offset);
		Vector2f pos2 = position2.getVirtual().copy().add(offset);
		float pixelWidth = VoicesGameBoard.getInstance().scaleScalar(1, true);
		
		this.renderOneColor(g, Color.white, pos1, pos2, 5*pixelWidth, 3*pixelWidth);
		this.renderOneColor(g, Color.black, pos1, pos2, 3*pixelWidth, pixelWidth);
		
	}
	
	private void renderOneColor(Graphics g, Color color, Vector2f pos1, Vector2f pos2, float circleWidth, float lineWidth)
	{
		g.setColor(color);
		g.setLineWidth(lineWidth);
		
		float deltaX = pos2.x - pos1.x;
		float deltaY = pos2.y - pos1.y;
		
		float avgX = (pos1.x + pos2.x) / 2;
		float avgY = (pos1.y + pos2.y) / 2;
		
		float normalX = avgX + deltaY / 4;
		float normalY = avgY - deltaX / 4;
		
		float arrow1X = (normalX + pos1.x) / 2;
		float arrow1Y = (normalY + pos1.y) / 2;
		float arrow2X = (normalX + pos2.x) / 2;
		float arrow2Y = (normalY + pos2.y) / 2;
		
		g.fillOval(pos1.x - circleWidth/2, pos1.y - circleWidth/2, circleWidth, circleWidth);
		if (getMode() != MODE_NOT_PLACED)
		{
			g.fillOval(pos2.x - circleWidth/2, pos2.y - circleWidth/2, circleWidth, circleWidth);
			g.drawLine(pos1.x, pos1.y, pos2.x, pos2.y);
			
			g.drawLine(avgX, avgY, normalX, normalY);
			g.drawLine(normalX, normalY, arrow1X, arrow1Y);
			g.drawLine(normalX, normalY, arrow2X, arrow2Y);
		}
	}
	
	public void mouseReleased(int button, int x, int y)
	{
		if (getMode() == MODE_NOT_PLACED)
		{
			setMode(MODE_HALF_PLACED);
		}
		else if (getMode() == MODE_HALF_PLACED)
		{
			setMode(MODE_FULLY_PLACED);
		}
	}
	
	public void mouseMoved(int mouseX, int mouseY, Vector2f offset)
	{
		float x = mouseX - offset.x;
		float y = mouseY - offset.y;
		switch (getMode())
		{
		case MODE_NOT_PLACED:
			position1.setVirtual(new Vector2f(x, y));
			position1.snapToGrid();
			break;
		case MODE_HALF_PLACED:
			position2.setVirtual(new Vector2f(x, y));
			position1.snapToGrid();
			position2.snapToGrid();
			break;
		}
	}
	
	public void snapToObstacle(VoicesObstacle o)
	{
		LinkedList<VoicesTextureObject> textureObjects = new LinkedList<VoicesTextureObject>();
		textureObjects.add(o);
		Vector2f offset = o.position.getPixel();
		
		switch (getMode())
		{
		case MODE_NOT_PLACED:
			position1.snapToTextureObjects(textureObjects, offset);
			position1.snapToWalls(textureObjects, offset);
			break;
		case MODE_HALF_PLACED:
			position2.snapToTextureObjects(textureObjects, offset);
			position2.snapToWalls(textureObjects, offset);
			break;
		}
	}
	
	public Vector2f delta()
	{
		Vector2f finalPos = position2.getPixel().copy();
		Vector2f initialPos = position1.getPixel().copy().scale(-1);
		
		return finalPos.add(initialPos);
	}
	
	public boolean collidedWithPosition(Vector2f posB1, Vector2f posB2, Vector2f offset, boolean priority)
	{
		float collisionRange = 0;
		if (priority)
		{
			collisionRange = 1;
		}
		
		Vector2f posA1 = position1.getPixel().copy().add(offset);
		Vector2f posA2 = position2.getPixel().copy().add(offset);
		
		float slopeA = (posA2.y - posA1.y) / (posA2.x - posA1.x);
		float slopeB = (posB2.y - posB1.y) / (posB2.x - posB1.x);
		
		float xIntersect;
		float yIntersect;
		if (Float.isNaN(slopeB) || slopeB == slopeA)
		{
			xIntersect = posB1.x;
			yIntersect = posB1.y;
		}
		else
		{
			if (!Float.isFinite(slopeA))
			{
				if (!Float.isFinite(slopeB))
				{
					return false;
				}
				else
				{
					xIntersect = posA1.x;
					yIntersect = (xIntersect - posB1.x) * slopeB + posB1.y;
				}
			}
			else
			{
				if (!Float.isFinite(slopeB))
				{
					xIntersect = posB1.x;
					yIntersect = (xIntersect - posA1.x) * slopeA + posA1.y;
				}
				else
				{
					float numerator1 = slopeA*posA1.x - slopeB*posB1.x;
					float numerator2 = posB1.y - posA1.y;
					float denominator = slopeA - slopeB;
					
					xIntersect = (numerator1 + numerator2) / denominator;
					yIntersect = (xIntersect - posA1.x) * slopeA + posA1.y;
				}
			}
		}
		
		float minXA = Math.min(posA1.x, posA2.x) - collisionRange;
		float maxXA = Math.max(posA1.x, posA2.x) + collisionRange;
		
		float minXB = Math.min(posB1.x, posB2.x) - collisionRange;
		float maxXB = Math.max(posB1.x, posB2.x) + collisionRange;
		
		float minYA = Math.min(posA1.y, posA2.y) - collisionRange;
		float maxYA = Math.max(posA1.y, posA2.y) + collisionRange;
		
		float minYB = Math.min(posB1.y, posB2.y) - collisionRange;
		float maxYB = Math.max(posB1.y, posB2.y) + collisionRange;
		
		boolean fitXA = xIntersect >= minXA && xIntersect <= maxXA;
		boolean fitXB = xIntersect >= minXB && xIntersect <= maxXB;
		
		boolean fitYA = yIntersect >= minYA && yIntersect <= maxYA;
		boolean fitYB = yIntersect >= minYB && yIntersect <= maxYB;
				
		return fitXA && fitXB && fitYA && fitYB;
	}
	
	public void writeObject(ObjectOutputStream out) throws IOException 
	{
		out.writeObject(position1);
		out.writeObject(position2);
	}

	public void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException 
	{
		this.position1 = (VoicesPositionVector)in.readObject();
		this.position2 = (VoicesPositionVector)in.readObject();
		setMode(MODE_FULLY_PLACED);
	}
	
	public void readObjectNoData() throws ObjectStreamException 
	{
		//I really need this function?
	}
}
