package voicesgame;

import java.io.File;
import java.util.LinkedList;
import java.util.Stack;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesStateTextureObjectAdder extends VoicesState
{
	public final int MODE_NORMAL = 0;
	public final int MODE_SCROLLING = 3;
	
	private VoicesTextureObjectTemplate tmp;
	private VoicesObstacle preview;

	
	private VoicesTextBox outputBox;
	private VoicesTextField nameField;
	private VoicesTextField pathField;
	private VoicesTextField widthField;
	private VoicesTextField heightField;
	
	private VoicesButton transparencyButton;
	private VoicesButton wallButton;
	private VoicesButton cancelButton;
	private VoicesButton keepButton;
	private VoicesButton resetButton;
	private VoicesButton saveButton;
	
	private VoicesWall currentWall;
		
	public VoicesStateTextureObjectAdder(int s)
	{
		super(s);
		
		addMenuObject(new VoicesTextBox(new Vector2f(0, 0), new Vector2f(508, 64), "info", Color.gray, "Object Name:"));
		addMenuObject(new VoicesTextBox(new Vector2f(0, 72), new Vector2f(508, 64), "info", Color.gray, "Texture path:"));
		addMenuObject(new VoicesTextBox(new Vector2f(0, 144), new Vector2f(508, 64), "info", Color.gray, "Width:"));
		addMenuObject(new VoicesTextBox(new Vector2f(0, 216), new Vector2f(508, 64), "info", Color.gray, "Height:"));
		
		outputBox = new VoicesTextBox(new Vector2f(0, 504), new Vector2f(508, 64), "info", Color.red, "");
		
		nameField = new VoicesTextField(new Vector2f(512, 0), new Vector2f(512, 64), "name", Color.green, "");
		pathField = new VoicesTextField(new Vector2f(512, 72), new Vector2f(512, 64), "path", Color.green, "");
		widthField = new VoicesTextField(new Vector2f(512, 144), new Vector2f(512, 64), "width", Color.green, "");
		heightField = new VoicesTextField(new Vector2f(512, 216), new Vector2f(512, 64), "height", Color.green, "");
		
		saveButton = new VoicesButton(new Vector2f(512, 504), new Vector2f(508, 64), "save", Color.green, "Save");
		transparencyButton = new VoicesButton(new Vector2f(0, 288), new Vector2f(508, 64), "transparent", Color.blue, "Transparent: no");
		wallButton = new VoicesButton(new Vector2f(0, 360), new Vector2f(508, 64), "wall", Color.yellow, "Add wall");
		cancelButton = new VoicesButton(new Vector2f(0, 360), new Vector2f(252, 64), "cancel", Color.red, "Cancel wall");
		keepButton = new VoicesButton(new Vector2f(256, 360), new Vector2f(252, 64), "keep", Color.green, "Keep wall");
		resetButton = new VoicesButton(new Vector2f(0, 432), new Vector2f(508, 64), "reset", Color.red, "Reset walls");
		
		addMenuObject(outputBox);
		addMenuObject(nameField);
		addMenuObject(pathField);
		addMenuObject(widthField);
		addMenuObject(heightField);
		
		addMenuObject(saveButton);
		addMenuObject(transparencyButton);
		addMenuObject(wallButton);
		addMenuObject(cancelButton);
		addMenuObject(keepButton);
		addMenuObject(resetButton);
		
		cancelButton.setVisibility(false);
		keepButton.setVisibility(false);
		
		tmp = new VoicesTextureObjectTemplate("", "", VoicesObstacle.class.getName(), 0);
		tmp.setTranslucent(false);
	}
	
	protected void updateMenuEvents(Stack<VoicesMenuEvent> s)
	{
		while (s.size() > 0)
		{
			VoicesMenuEvent e = s.pop();
			VoicesMenuObject source = e.getMenuObject();
			
			boolean errors = checkForErrors();
			if (!errors)
			{
				if (source == saveButton)
				{
					tmp = new VoicesTextureObjectTemplate(preview);
					tmp.saveToFile();
					VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_TEXTURE_OBJECT_PICKER);
				}
				else if (source == wallButton)
				{
					preview.addWall();
					currentWall = preview.getWalls().getLast();
					cancelButton.setVisibility(true);
					keepButton.setVisibility(true);
					wallButton.setVisibility(false);
				}
				else if (source == cancelButton)
				{
					preview.getWalls().remove(currentWall);
					currentWall = null;
					
					cancelButton.setVisibility(false);
					keepButton.setVisibility(false);
					wallButton.setVisibility(true);
				}
				else if (source == keepButton)
				{
					currentWall = null;
					
					cancelButton.setVisibility(false);
					keepButton.setVisibility(false);
					wallButton.setVisibility(true);
				}
				else if (source == resetButton)
				{
					preview.setWalls(new LinkedList<VoicesWall>());
				}
				else if (source == transparencyButton)
				{
					boolean t = !preview.isTranslucent();
					preview.setTranslucent(t);
					tmp.setTranslucent(t);
					this.setTransparencyButtonContents(t);
				}
			}
		}
	}

	protected void updatePlayerEvents(Stack<VoicesPlayerEvent> s)
	{
	
	}

	protected void updateEditorEvents(Stack<VoicesEditorEvent> s)
	{
		
	}

	private void setTransparencyButtonContents(boolean i)
	{
		if (i)
		{
			transparencyButton.setContent("Transparent: yes");
		}
		else
		{
			transparencyButton.setContent("Transparent: no");
		}
	}
	
	public boolean checkForErrors()
	{
		outputBox.setTextColor(Color.red);
		tmp.setName(nameField.getContent());
		if(nameField.getContent() == "")
		{
			outputBox.setContent("Invalid name");
			return true;
		}
		
		String path = "res/obstacles/" + pathField.getContent();
		tmp.setTextureFolderName(path);
		File f = new File(path + "/normal");
		if(!f.exists())
		{
			outputBox.setContent("Invalid texture path");
			return true;
		}
		
		try
		{
			Vector2f size = new Vector2f();
			size.x = Integer.parseInt(widthField.getContent());
			size.y = Integer.parseInt(heightField.getContent());
			tmp.setSize(size);
		}
		catch (Exception e)
		{
			outputBox.setContent("Invalid size");
			return true;
		}
		
		try
		{
			LinkedList<VoicesWall> tmpWalls = null;
			if (preview != null)
			{
				tmpWalls = preview.getWalls();
			}
			
			preview = (VoicesObstacle)VoicesTextureObjectFactory.createVoicesTextureObject(tmp, new Vector2f(512, 288));
			preview.setMode(VoicesTextureObject.MODE_ADDER);
			preview.setListener(this);
			preview.init(VoicesGameBoard.getInstance().game.getContainer(), VoicesGameBoard.getInstance().game);
			
			if (tmpWalls != null)
			{
				preview.setWalls(tmpWalls);
			}
		}
		catch (Exception e)
		{
			outputBox.setContent("Preview failed");
			super.getMenuObjects().remove(preview);
			return true;
		}
		
		outputBox.setContent("No errors");
		outputBox.setTextColor(Color.green);
		return false;
	}
	
	public void enter(GameContainer gc, StateBasedGame sbg) throws SlickException
	{
		super.enter(gc, sbg);
		
		preview = null;
		tmp = VoicesGameBoard.getInstance().getTextureObjectTemplate();
		if (tmp == null)
		{
			tmp = new VoicesTextureObjectTemplate("", "", VoicesObstacle.class.getName(), 0);
			tmp.setTranslucent(false);
			
			outputBox.setContent("Object Created");
			nameField.setContent("");
			pathField.setContent("");
			widthField.setContent("");
			heightField.setContent("");
		}
		else
		{
			outputBox.setContent("Object loaded");
			nameField.setContent(tmp.getName());
			String[] fullPath = tmp.getTextureFolderName().split("/");
			pathField.setContent(fullPath[2]);
			widthField.setContent(Integer.toString((int)tmp.getSize().x));
			heightField.setContent(Integer.toString((int)tmp.getSize().y));
			this.setTransparencyButtonContents(tmp.isTranslucent());
			this.checkForErrors();
		}
		
		cancelButton.setVisibility(false);
		keepButton.setVisibility(false);
		wallButton.setVisibility(true);
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException
	{
		super.render(gc, sbg, g);
		if(preview != null)
		{
			preview.render(gc, sbg, g, false);
		}
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException
	{
		super.update(gc, sbg, delta);
		if(preview != null)
		{
			preview.update(gc, sbg, delta);
		}
	}
	
	public void mouseReleased(int button, int x, int y) 
	{
		super.mouseReleased(button, x, y);
		if(preview != null)
		{
			preview.mouseReleased(button, x, y);
		}
	}
	public void mouseMoved(int oldx, int oldy, int x, int y) 
	{
		super.mouseMoved(oldx, oldy, x, y);
		if(preview != null)
		{
			preview.mouseMoved(oldx, oldy, x, y);
			for (VoicesWall w : preview.getWalls())
			{
				w.snapToObstacle(preview);
			}
		}
	}
}
