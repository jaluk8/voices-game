package voicesgame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public abstract class VoicesLevelBasedState extends VoicesState
{
	public final int MODE_NORMAL = 0;
	
	private int pickingState;
	
	protected VoicesLevel level;
	public VoicesLevel getLevel() { return level; }
	public void setLevel(VoicesLevel alvl)
	{
		level = alvl;
		level.setListener(this);
	}
	
	public VoicesLevelBasedState(int s, int aPickingState) 
	{
		super(s);
		pickingState = aPickingState;
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException
	{
		if (level != null)
		{
			level.render(gc, sbg, g);
			for (VoicesMenuObject o : menuObjects)
			{
				o.render(gc, sbg, g, false);
			}
		}
	}

	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException
	{
		if (level != null)
		{
			level.update(gc, sbg, delta);
			for (VoicesMenuObject o : menuObjects)
			{
				o.update(gc, sbg, delta);
			}
		}
		updateEvents();
	}
	
	public void enter(GameContainer gc, StateBasedGame sbg)
	{
		try
		{
			init(gc, sbg);
		}
		catch (SlickException e1)
		{
			e1.printStackTrace();
		}
		
		if (VoicesGameBoard.getInstance().getLevelTemplate() == null)
		{
			sbg.enterState(pickingState);
		}
		else
		{
			try 
			{
				this.setLevel(new VoicesLevel(VoicesGameBoard.getInstance().getLevelTemplate(), this));
			} 
			catch (SlickException e) 
			{
				e.printStackTrace();
			}
		}
	}
	
	public void save()
	{
		VoicesLevelTemplate vlt = new VoicesLevelTemplate(level);
		vlt.saveToFile();
		VoicesGameBoard.getInstance().loadLevelTemplates();
	}
	
	public void mousePressed(int button, int x, int y)
	{
		super.mousePressed(button, x, y);
		for (VoicesTextureObject o : level.getTextureObjects())
		{
			o.mousePressed(button, x, y);
		}
	}
	public void mouseReleased(int button, int x, int y) 
	{
		super.mouseReleased(button, x, y);
		for (VoicesTextureObject o : level.getTextureObjects())
		{
			o.mouseReleased(button, x, y);
		}
	}
	public void mouseMoved(int oldx, int oldy, int x, int y) 
	{
		super.mouseMoved(oldx, oldy, x, y);
		for (VoicesTextureObject o : level.getTextureObjects())
		{
			o.mouseMoved(oldx, oldy, x, y);
		}
	}
	public void keyPressed(int key, char c) 
	{
		super.keyPressed(key, c);
		for (VoicesTextureObject o : level.getTextureObjects())
		{
			try 
			{
				o.keyPressed(key, c);
			} 
			catch (SlickException e) 
			{
				e.printStackTrace();
			}
		}
	}
	public void keyReleased(int key, char c) 
	{
		super.keyReleased(key, c);
		for (VoicesTextureObject o : level.getTextureObjects())
		{
			o.keyReleased(key, c);
		}
	}
}
