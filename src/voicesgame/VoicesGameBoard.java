package voicesgame;

import java.io.File;
import java.util.LinkedList;
import java.util.Random;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.geom.Vector2f;

public class VoicesGameBoard 
{
	public VoicesGame game;
	
	public final int DEFAULT_FONT_SIZE = 128;
	private UnicodeFont defaultFont;
	public UnicodeFont getDefaultFont() throws SlickException
	{
		if (defaultFont == null)
		{
			defaultFont = generateFont(DEFAULT_FONT_SIZE);
		}
		return defaultFont;
	}
	
	private VoicesLevelTemplate currentLevelTemplate;
	public VoicesLevelTemplate getLevelTemplate() { return currentLevelTemplate; }
	public void setLevelTemplate (VoicesLevelTemplate aTemp) throws SlickException { currentLevelTemplate = aTemp; }
	
	private VoicesTextureObjectTemplate currentTextureObjectTemplate;
	public VoicesTextureObjectTemplate getTextureObjectTemplate() { return currentTextureObjectTemplate; }
	public void setTextureObjectTemplate (VoicesTextureObjectTemplate aTemp) { currentTextureObjectTemplate = aTemp; }
	
	private LinkedList<VoicesLevelTemplate> levelTemplates;
	public LinkedList<VoicesLevelTemplate> getLevelTemplates() { return levelTemplates; }
	public void setLevelTemplates (LinkedList<VoicesLevelTemplate> aTemp) throws SlickException { levelTemplates = aTemp; }
	
	private LinkedList<VoicesTextureObjectTemplate> textureObjectTemplates;
	public LinkedList<VoicesTextureObjectTemplate> getTextureObjectTemplates() { return textureObjectTemplates; }
	public void setTextureObjectTemplates (LinkedList<VoicesTextureObjectTemplate> aTemp) throws SlickException { textureObjectTemplates = aTemp; }
	
	private LinkedList<VoicesTemplate> specialTemplates;
	public LinkedList<VoicesTemplate> getSpecialTemplates()
	{
		if (specialTemplates == null)
		{
			specialTemplates = new LinkedList<VoicesTemplate>();
			specialTemplates.add(new VoicesTextureObjectTemplate("Health voice", "res/entities/voice", VoicesWhispHealth.class.getName(), 1));
			specialTemplates.add(new VoicesTextureObjectTemplate("Boost voice", "res/entities/voice", VoicesWhispBoost.class.getName(), 2));
			specialTemplates.add(new VoicesTextureObjectTemplate("Bridge voice", "res/entities/voice", VoicesWhispBridge.class.getName(), 3));
			specialTemplates.add(new VoicesTextureObjectTemplate("Attack voice", "res/entities/voice", VoicesWhispAttack.class.getName(), 4));
		}
		return specialTemplates;
	}

	public final float PIXELS_PER_SCREEN_HEIGHT = 576;
	public final float PIXELS_PER_SCREEN_WIDTH = 1024;
	public final float SNAP_DISTANCE = 4;
	
	private Vector2f scroll = new Vector2f(0, 0);
	public Vector2f getScroll() { return scroll.copy(); }
	public Vector2f getVirtualScroll()
	{
		return scaleVector(scroll);
	}
	public void setScroll(Vector2f aScroll) { scroll = aScroll; }
	
	private Random random = new Random();
	public Random getRandom() { return random; }
	public void setRandom(Random aRand) { random = aRand; }
	
	private float yScale;
	public float getYScale() { return yScale; }
	
	private float xScale;
	public float getXScale() { return xScale; }
	
	private static VoicesGameBoard instance;
	private VoicesGameBoard() 
	{
		setScales();
	}
	public static VoicesGameBoard getInstance()
	{
		if (instance == null)
		{
			instance = new VoicesGameBoard();
			instance.loadLevelTemplates();
			instance.loadTextureObjectTemplates();
		}
		return instance;
	}
	
	public static VoicesPositionVector getMousePosition()
	{
		float x = Mouse.getX();
		float y = Display.getHeight() - Mouse.getY();
		VoicesPositionVector pos = new VoicesPositionVector();
		pos.setScrolled(new Vector2f(x, y));
		return pos;
	}
	
	public static VoicesLevel getCurrentLevel()
	{
		VoicesGame g = getInstance().game;
		if (g.getCurrentState() instanceof VoicesLevelBasedState)
		{
			VoicesLevelBasedState s = (VoicesLevelBasedState) g.getCurrentState();
			return s.level;
		}
		else
		{
			return null;
		}
	}
	
	public void setScales()
	{
		yScale = Display.getHeight() / PIXELS_PER_SCREEN_HEIGHT;
		xScale = Display.getWidth() / PIXELS_PER_SCREEN_WIDTH;
	}
	
	public Vector2f scaleVector(Vector2f aVec)
	{
		Vector2f copiedVec = aVec.copy();
		copiedVec.x *= xScale;
		copiedVec.y *= yScale;
		return copiedVec;
	}
	
	public Vector2f antiScaleVector(Vector2f aVec)
	{
		Vector2f copiedVec = aVec.copy();
		copiedVec.x /= xScale;
		copiedVec.y /= yScale;
		return copiedVec;
	}
	
	public Vector2f scrollVector(Vector2f aVec)
	{
		Vector2f copiedVec = aVec.copy();
		copiedVec.x += getVirtualScroll().x;
		copiedVec.y -= getVirtualScroll().y;
		return copiedVec;
	}
	
	public float scaleScalar(float aFloat, boolean useWidth)
	{
		if (useWidth)
		{
			aFloat *= xScale;
		}
		else
		{
			aFloat *= yScale;
		}
		return aFloat;
	}
	
	@SuppressWarnings("unchecked")
	public UnicodeFont generateFont(int size) throws SlickException
	{
		UnicodeFont f = new UnicodeFont(new java.awt.Font("res/font/LiberationSerif-Regular.ttf", java.awt.Font.PLAIN, size));
		f.addAsciiGlyphs();
		f.getEffects().add(new ColorEffect());
		f.loadGlyphs();
		return f;
	}
	
	public void loadLevelTemplates()
	{		
		levelTemplates = new LinkedList<VoicesLevelTemplate>();
		File directory = new File("templates/level");
		for (File f : directory.listFiles())
		{
			VoicesLevelTemplate tmp = VoicesLevelTemplate.loadFromFile(f.getName());
			levelTemplates.add(tmp);
		}
		
		if (currentLevelTemplate != null)
		{
			currentLevelTemplate = VoicesLevelTemplate.loadFromFile(currentLevelTemplate.getId()+".vtmp");
		}
	}
	
	public void loadTextureObjectTemplates()
	{
		textureObjectTemplates = new LinkedList<VoicesTextureObjectTemplate>();
		File directory = new File("templates/textureObject");
		for (File f : directory.listFiles())
		{
			VoicesTextureObjectTemplate tmp = VoicesTextureObjectTemplate.loadFromFile(f.getName());
			textureObjectTemplates.add(tmp);
		}
	}
}
