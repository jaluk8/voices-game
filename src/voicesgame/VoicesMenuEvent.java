package voicesgame;

public class VoicesMenuEvent extends VoicesEvent
{
	private VoicesMenuObject object;
	public VoicesMenuObject getMenuObject(){ return object; }
	
	public VoicesMenuEvent(VoicesMenuObject o)
	{
		super(0);
		object = o;
	}
}
