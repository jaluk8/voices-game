package voicesgame;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

public class VoicesGame extends StateBasedGame
{	
	public static final String GAME_NAME = "Voices";
	
	static final int STATE_MAIN_MENU = 0;
	static final int STATE_PLAY = 1;
	static final int STATE_LEVEL_EDITOR = 2;
	static final int STATE_LEVEL_PICKER = 3;
	static final int STATE_LEVEL_PLAY_PICKER = 4;
	static final int STATE_LEVEL_EDITOR_ADDER = 5;
	static final int STATE_TEXTURE_OBJECT_PICKER = 6;
	static final int STATE_TEXTURE_OBJECT_ADDER = 7;
	static final int STATE_LOSE = 8;
	static final int STATE_WIN = 9;
	static final int STATE_SPECIAL_PICKER = 10;
	
	
	public VoicesGame(String name) throws SlickException
	{
		super(name);
		this.addState(new VoicesStateMainMenu(STATE_MAIN_MENU));
		this.addState(new VoicesStateLevelEditor(STATE_LEVEL_EDITOR));
		this.addState(new VoicesStatePlay(STATE_PLAY));
		this.addState(new VoicesStateLevelPicker(STATE_LEVEL_PICKER));
		this.addState(new VoicesStatePlayPicker(STATE_LEVEL_PLAY_PICKER));
		this.addState(new VoicesStateLevelAdder(STATE_LEVEL_EDITOR_ADDER));
		this.addState(new VoicesStateTextureObjectPicker(STATE_TEXTURE_OBJECT_PICKER));
		this.addState(new VoicesStateTextureObjectAdder(STATE_TEXTURE_OBJECT_ADDER));
		this.addState(new VoicesStateLose(STATE_LOSE));
		this.addState(new VoicesStateWin(STATE_WIN));
		this.addState(new VoicesStateSpecialPicker(STATE_SPECIAL_PICKER));

		VoicesGameBoard.getInstance().game = this;
		VoicesGameBoard.getInstance().loadLevelTemplates();
	}
	
	public void initStatesList(GameContainer gc) throws SlickException
	{
		this.getState(STATE_MAIN_MENU).init(gc, this);
		this.getState(STATE_LEVEL_EDITOR).init(gc, this);
		this.getState(STATE_PLAY).init(gc, this);
		this.getState(STATE_LEVEL_PICKER).init(gc, this);
		this.getState(STATE_LEVEL_PLAY_PICKER).init(gc, this);
		this.getState(STATE_LEVEL_EDITOR_ADDER).init(gc, this);
		this.getState(STATE_TEXTURE_OBJECT_PICKER).init(gc, this);
		this.getState(STATE_TEXTURE_OBJECT_ADDER).init(gc, this);
		this.getState(STATE_LOSE).init(gc, this);
		this.getState(STATE_WIN).init(gc, this);
		this.getState(STATE_SPECIAL_PICKER).init(gc, this);
		
		this.enterState(STATE_MAIN_MENU);
	}
	
	public static void main(String[] args) 
	{
		AppGameContainer appgc;
		try
		{
			appgc = new AppGameContainer(new VoicesGame(GAME_NAME));
			appgc.setDisplayMode(appgc.getScreenWidth(), appgc.getScreenHeight(), true);
			appgc.setTargetFrameRate(60);
			appgc.start();
		} 
		catch(SlickException e)
		{
			e.printStackTrace();
		}
	}

	public void enterState(int aState, int aMode)
	{
		VoicesState vs = (VoicesState)getState(aState);
		vs.setMode(aMode);
		super.enterState(aState);
	}
	
}
