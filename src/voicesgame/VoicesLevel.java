package voicesgame;

import java.util.LinkedList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesLevel
{
	private String templateName;
	public String getTemplateName() { return templateName; }
	
	private int templateId;
	public int getTemplateId() { return templateId; }
	
	private boolean isInitialized = false;
	public boolean getInitialized() { return isInitialized; }
	
	private VoicesPlayer player;
	public VoicesPlayer getPlayer() { return player; }
	public void setPlayer(VoicesPlayer aPlayer) { player = aPlayer; }
	
	protected VoicesState listener;
	public VoicesState getlistener() { return listener; }
	public void setListener(VoicesState moal) { listener = moal; }
	
	private LinkedList<VoicesTextureObject> killList = new LinkedList<VoicesTextureObject>();
	
	private LinkedList<VoicesTextureObject> textureObjects = new LinkedList<VoicesTextureObject>();
	public LinkedList<VoicesTextureObject> getTextureObjects() { return textureObjects; }
	public void setTextureObjects(LinkedList<VoicesTextureObject> someEnts) { textureObjects = someEnts; } //The number of arguments here can never increase because the Entwives have been lost
																							               //This joke was funnier when the type was VoicesEntity... I should remove this, but I refuse!
	public VoicesLevel(VoicesLevelTemplate tmp, VoicesState al) throws SlickException 
	{
		templateName = tmp.getName();
		templateId = tmp.getId();
		
		listener = al;
		
		player = new VoicesPlayer(tmp.getPlayerPos());
		addTextureObject(player);
		for (VoicesTextureObjectReference ref : tmp.getObjectRefs())
		{
			addTextureObject(ref.createVoicesTextureObject(this));
		}
	}

	public void addTextureObject(VoicesTextureObject anObject)
	{
		textureObjects.add(anObject);
		anObject.setListener(listener);
		rescaleVirtualVectors();
		
		if (anObject.getClass().getName().equals(VoicesPlayer.class.getName()))
		{
			player = (VoicesPlayer)anObject;
		}
	}
	
	public void removeTextureObject(VoicesTextureObject o)
	{
		killList.add(o);
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException 
	{
		isInitialized = true;
		for (VoicesTextureObject o : textureObjects)
		{
			o.init(gc, sbg);
		}
		rescaleVirtualVectors();
	}

	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException 
	{
		if (!isInitialized)
		{
			this.init(gc, sbg);
		}
		for (VoicesTextureObject e : textureObjects)
		{
			e.render(gc, sbg, g, true);
		}
	}

	@SuppressWarnings("unchecked")
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException 
	{
		killObjects();
		if (!isInitialized)
		{
			this.init(gc, sbg);
		}
		
		LinkedList<VoicesTextureObject> vtos = (LinkedList<VoicesTextureObject>) textureObjects.clone();
		while(vtos.size() > 0)
		{
			VoicesTextureObject e = vtos.pop();
			e.update(gc, sbg, delta);
		}
	}
	
	public void rescaleVirtualVectors()
	{
		VoicesGameBoard.getInstance().setScales();
		for (VoicesTextureObject e : textureObjects)
		{
			try 
			{
				e.rescaleVirtualVectors();
			} 
			catch (SlickException e1) 
			{
				e1.printStackTrace();
			}
		}
	}
	
	private void killObjects()
	{
		for (VoicesTextureObject o : killList)
		{
			textureObjects.remove(o);
		}
		killList.clear();
	}
	
}
