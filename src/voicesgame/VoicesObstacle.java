package voicesgame;

import java.util.LinkedList;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesObstacle extends VoicesTextureObject
{	
	private LinkedList<VoicesWall> walls = new LinkedList<VoicesWall>();
	public LinkedList<VoicesWall> getWalls() { return walls; }
	public void setWalls(LinkedList<VoicesWall> someWalls) { walls = someWalls; } //<Obscure>Won't ever update their damn game.</Obscure>
	
	public VoicesObstacle(Vector2f aPos, Vector2f aSize, String aTextureName, String aTemplateName, int aTemplateId, LinkedList<VoicesWall> w, boolean invis)
	{
		super(aPos, aSize, aTextureName, aTemplateName, aTemplateId);
		walls = w;
		setTranslucent(invis);
	}
	
	@SuppressWarnings("unchecked")
	public VoicesTextureObject makeCopy()
	{
		LinkedList<VoicesWall> w = (LinkedList<VoicesWall>)getWalls().clone();
		VoicesObstacle o2 = new VoicesObstacle(position.getPixel().copy(), size.getPixel().copy(), getTextureFolderName(), getTemplateName(), getTemplateId(), w, isTranslucent());
		return o2;
	}

	public void addWall()
	{
		VoicesWall w = new VoicesWall();
		walls.add(w);
	}
	
	public void loadTextureFromMode()
	{
		String filePath = getTextureFolderName();
		switch(getMode())
		{
		case MODE_NORMAL:
			filePath += "/normal";
			break;
		case MODE_PREVIEW:
			filePath = "res/obstacles/preview";
			break;
		case MODE_IN_EDITOR:
			filePath += "/normal";
			break;
		case MODE_ADDER:
			filePath += "/normal";
			break;
		}
		setSourceAnimation(filePath);
	}
	
	public void mouseReleased(int button, int x, int y)
	{
		if (isHit(new Vector2f(x, y), getMode() != MODE_ADDER))
		{
			listener.addEditorEvent(this, button);
			if (getMode() == MODE_IN_EDITOR || getMode() == MODE_ADDER)
			{
				for (VoicesWall w : walls)
				{
					w.mouseReleased(button, x, y);
				}
			}
		}
	}
	public void mouseMoved(int oldx, int oldy, int x, int y)
	{
		if (getMode() == MODE_PREVIEW)
		{
			position.setScrolled(new Vector2f(x, y));
			position.snapToGrid();
		}
		if (getMode() == MODE_IN_EDITOR || getMode() == MODE_ADDER)
		{
			for (VoicesWall w : walls)
			{
				w.mouseMoved(x, y, position.getVirtual());
			}
		}
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g, boolean scroll) throws SlickException
	{
		super.render(gc, sbg, g, scroll);
		if (getMode() == MODE_IN_EDITOR || getMode() == MODE_ADDER)
		{
			for (VoicesWall w : walls)
			{
				if (!w.position1.isNull())
				{
					if (scroll)
					{
						w.render(gc, sbg, g, position.getScrolled());
					}
					else
					{
						w.render(gc, sbg, g, position.getVirtual());
					}
				}
			}
		}
	}
	
	public void snapToTextureObjects(LinkedList<VoicesTextureObject> textureObjects, Vector2f offset)
	{
		super.snapToTextureObjects(textureObjects, offset);
		for (VoicesWall w : this.getWalls())
		{
			position.snapToWalls(textureObjects, w.position1.getPixel().add(offset));
			position.snapToWalls(textureObjects, w.position2.getPixel().add(offset));
		}
	}
}
