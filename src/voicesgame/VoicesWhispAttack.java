package voicesgame;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesWhispAttack extends VoicesWhisp
{
	private static Color teamColor(int t)
	{
		Color c;
		
		switch (t)
		{
		case TEAM_PLAYER:
			c = Color.white.darker(0.05f);
			break;
		case TEAM_ENEMY:
			c = Color.white.darker(0.90f);
			break;
		default:
			c = Color.gray;
		}
		
		return c;
	}
	
	public void setTeam(int t)
	{
		super.setTeam(t);
		setColor(teamColor(t));
		updatePlayerHud();
	}
	
	public VoicesWhispAttack(Vector2f aPos, int anId, int aTeam)
	{
		super(aPos, teamColor(aTeam), anId, aTeam);
	}
	public VoicesWhispAttack(Vector2f aPos, int anId)
	{
		super(aPos, teamColor(TEAM_NO_ONE), anId);
	}

	public VoicesTextureObject makeCopy()
	{
		return new VoicesWhispAttack(position.getPixel().copy(), getTemplateId(), team);
	}
	boolean tmp = true;
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException 
	{
		super.update(gc, sbg, delta);
		
		if (getMode() == MODE_CHASING && collidesWith(target) && target instanceof VoicesEntity)
		{
			((VoicesEntity)target).hit();
			if (this.getTeam() == TEAM_PLAYER)
			{
				this.setTeam(TEAM_ENEMY);
				this.attack(owner);
			}
			else
			{
				this.setMode(MODE_DEATH);
			}
		}
		else if (getMode() == MODE_CHASING && target instanceof VoicesEntity)
		{
			VoicesEntity entTarget = (VoicesEntity) target;
			if (entTarget.getMode() == VoicesEntity.MODE_DEATH)
			{
				this.returnToOwner();
			}
		}
	}
	
	public void attack(VoicesEntity e)
	{
		this.chaseVTO(e);
		e.notifyOfAttack(this);
	}
}
