package voicesgame;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public abstract class VoicesWhisp extends VoicesEntity
{
	static final int MODE_NORMAL = 0;
	static final int MODE_FOLLOWING = 1;
	static final int MODE_CHASING = 2;
	
	private Random rnd = new Random();

	private final float TURN_SPEED = 0.5f;
	private final float MAX_SPEED = 5f;
	private final float MIN_SPEED = 0.1f;
	private final float MIN_ACCELERATION = 0.0001f;
	private final float BRAKE_ACCELERATION = 0.001f;
	private final float MAX_ACCELERATION = 0.01f;
	private final float MAX_ARRIVAL_TIME = 400f;
	private final float MIN_ANGLE_TO_TARGET = 1f;
	
	static final String TEXTURE_NAME = "res/entities/voice";
	
	protected VoicesEntity owner;
	public VoicesEntity getOwner() { return owner; }
	public void setOwner(VoicesEntity own)
	{
		owner = own; //Ha! Get owned!
		setTeam(owner.getTeam());
	}	
	
	protected VoicesTextureObject target;
	protected Vector2f targetOffset;
	public void setTarget(VoicesTextureObject vto, Vector2f off)
	{
		target = vto;
		targetOffset = off;
	}
	
	public VoicesWhisp(Vector2f aPos, Color c, int anId)
	{
		super(aPos, TEXTURE_NAME, anId, VoicesEntity.TEAM_NO_ONE);
		this.setGravity(false);
		this.setCollide(false);
		color = c;
	}
	public VoicesWhisp(Vector2f aPos, Color c, int anId, int aTeam)
	{
		super(aPos, TEXTURE_NAME, anId, aTeam);
		this.setGravity(false);
		this.setCollide(false);
		color = c;
	}

	public void loadTextureFromMode()
	{
		String filePath = getTextureFolderName();
		boolean looping = true;
		switch(getMode())
		{
		case MODE_NORMAL:
			filePath += "/normal";
			break;
		case MODE_DEATH:
			filePath += "/death";
			looping = false;
			break;
		case MODE_FOLLOWING:
			filePath += "/normal";
			break;
		case MODE_CHASING:
			filePath += "/normal";
			break;
		case MODE_IN_EDITOR:
			filePath += "/normal";
			break;
		case MODE_PREVIEW:
			filePath = "res/obstacles/preview";
			break;
		}
		setSourceAnimation(filePath);
		sourceAnimation.setLooping(looping);
	}
	
	public void setMode(int aMode)
	{
		super.setMode(aMode);
		
		if (aMode == MODE_DEATH)
		{
			if (owner instanceof VoicesPlayer)
			{
				((VoicesPlayer) owner).updatePlayStateWhispHud();
			}
		}
	}
	
	protected Vector2f targetPosition()
	{
		Vector2f pos = target.position.getPixel().copy();
		pos.add(targetOffset);
		return pos;
	}
	
	private Vector2f deltaPosToTarget()
	{
		Vector2f targetPos = targetPosition();
		Vector2f whispPos = position.getPixel().copy().add(size.getPixel().copy().scale(0.5f));
		
		Float deltaX = targetPos.x - whispPos.x;
		Float deltaY = targetPos.y - whispPos.y;
		return new Vector2f(deltaX, deltaY);
	}
	
	private double differenceInAngleToVelocity()
	{
		double deltaTheta = deltaPosToTarget().getTheta() - velocity.getTheta();
		while (deltaTheta > 180)
		{
			deltaTheta -= 360;
		}
		while (deltaTheta < -180)
		{
			deltaTheta += 360;
		}
		return deltaTheta;
	}
	
	private void chase(int delta)
	{
		Vector2f deltaPos = deltaPosToTarget();
		
		double deltaTheta = differenceInAngleToVelocity();
		if (deltaTheta > 0)
		{
			velocity.add(TURN_SPEED * delta);
			if (differenceInAngleToVelocity() < 0)
			{
				velocity.setTheta(deltaPos.getTheta());
			}
		}
		else if (deltaTheta < 0)
		{
			velocity.add(-TURN_SPEED * delta);
			if (differenceInAngleToVelocity() > 0)
			{
				velocity.setTheta(deltaPos.getTheta());
			}
		}
		
		if (Math.abs(differenceInAngleToVelocity()) < MIN_ANGLE_TO_TARGET)
		{
			float newSpeed = velocity.length();
			if (arrivalTime() <= MAX_ARRIVAL_TIME)
			{
				newSpeed += MIN_ACCELERATION * delta;
			}
			else
			{
				newSpeed += MAX_ACCELERATION * delta;
			}
			setSpeed(newSpeed);
		}
	}
	
	private double arrivalTime()
	{
		float v = velocity.length();
		float a = BRAKE_ACCELERATION;
		float x = deltaPosToTarget().length();
		
		return (-v + Math.sqrt(v*v + a*x)) / a;
	}
	
	private void slowWhenNear(int delta)
	{
		float v = velocity.length();
		float a = BRAKE_ACCELERATION;
		
		double timeToArrival = arrivalTime();
		double timeNeededToBrake = (v - MIN_SPEED)/a;
		
		float newSpeed = v;
		if (timeNeededToBrake >= timeToArrival)
		{
			newSpeed = v - a * delta;
			if (newSpeed < MIN_SPEED)
			{
				newSpeed = MIN_SPEED;
			}
		}
		
		setSpeed(newSpeed);
	}
	
	private void setSpeed(float speed)
	{
		velocity.scale(speed / velocity.length());
	}
	
	private void controlSpeed()
	{
		if (velocity.length() > MAX_SPEED)
		{
			setSpeed(MAX_SPEED);
		}
		else if (velocity.length() != 0 && velocity.length() < MIN_SPEED)
		{
			setSpeed(MIN_SPEED);
		}
	}
	
	public boolean collidesWith(VoicesTextureObject vto)
	{
		Vector2f p = position.getVirtual();
		Vector2f s = size.getVirtual();
		Vector2f[] corners = 
		{
			new Vector2f(p.x      , p.y      ),
			new Vector2f(p.x + s.x, p.y      ),
			new Vector2f(p.x      , p.y + s.y),
			new Vector2f(p.x + s.x, p.y + s.y)
		};
		
		for (Vector2f corner : corners)
		{
			if (vto.isHit(corner, false))
			{
				return true;
			}
		}
		
		return false;
	}
	
	public void returnToOwner()
	{
		Vector2f ownSize = owner.size.getPixel().copy();
		this.setTarget(owner, ownSize.scale(0.5f));
		this.setMode(MODE_FOLLOWING);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException 
	{
		if (getMode() == MODE_FOLLOWING)
		{
			chase(delta);
			controlSpeed();
			slowWhenNear(delta);
		}
		else if (getMode() == MODE_CHASING)
		{
			chase(delta);
			controlSpeed();
		}
		else if (getMode() == MODE_NORMAL)
		{
			for (VoicesTextureObject vto : VoicesGameBoard.getCurrentLevel().getTextureObjects())
			{
				if (vto instanceof VoicesPlayer)
				{
					if (collidesWith(vto))
					{
						setMode(MODE_FOLLOWING);
						setTarget(vto, new Vector2f(vto.size.getPixel().x / 2, vto.size.getPixel().y / 2));
						setVelocity(deltaPosToTarget());
						setSpeed(MIN_SPEED);
						
						VoicesPlayer player = (VoicesPlayer)vto;
						player.addWhisp(this);
						setOwner(player);
						return;
					}
				}
			}
		}
		else if (getMode() == MODE_DEATH && sourceAnimation.isDone())
		{
			die();
		}
		
		controlSpeed();
		
		if (getMode() != MODE_IN_EDITOR && getMode() != MODE_PREVIEW)
		{
			position.getPixel().add(new Vector2f(rnd.nextFloat()-0.5f, rnd.nextFloat()-0.5f));
		}
		
		super.update(gc, sbg, delta);
	}
	
	public void hit()
	{
		setMode(MODE_DEATH);
	}
	
	protected void chaseVTO(VoicesTextureObject vto)
	{
		Vector2f vtoSize = vto.size.getPixel().copy();
		this.setTarget(vto, vtoSize.scale(0.5f));
		this.setMode(MODE_CHASING);
	}
	
	protected void die()
	{
		VoicesGameBoard.getCurrentLevel().getTextureObjects().remove(this);
		if (owner != null)
		{
			owner.getWhisps().remove(this);
		} 
		updatePlayerHud();
	}
	
	protected void updatePlayerHud()
	{
		if (target instanceof VoicesPlayer)
		{
				((VoicesPlayer)target).updatePlayStateWhispHud();
		}
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g, boolean scroll) throws SlickException
	{
		Vector2f virtualPos = position.getVirtual();
		Vector2f virtualSize = size.getVirtual();
		
		if (scroll)
		{
			virtualPos = position.getScrolled();
		}
		
		float x_min = -virtualSize.x;
		float x_max = VoicesGameBoard.getInstance().scaleScalar(VoicesGameBoard.getInstance().PIXELS_PER_SCREEN_WIDTH, true);
		float y_min = -virtualSize.y;
		float y_max = VoicesGameBoard.getInstance().scaleScalar(VoicesGameBoard.getInstance().PIXELS_PER_SCREEN_HEIGHT, false);
		if (virtualPos.x < x_max && virtualPos.y < y_max && virtualPos.x > x_min && virtualPos.y > y_min)
		{
			g.drawImage(sourceAnimation.getTexture(), virtualPos.x, virtualPos.y, virtualPos.x + virtualSize.x, virtualPos.y + virtualSize.y, 0, 0, size.getPixel().x, size.getPixel().y, color);
		}
	}
}
