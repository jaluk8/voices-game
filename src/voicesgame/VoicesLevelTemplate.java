package voicesgame;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.util.LinkedList;

import org.newdawn.slick.geom.Vector2f;

@SuppressWarnings("serial")
public class VoicesLevelTemplate extends VoicesTemplate
{
	private Vector2f playerPos;
	public Vector2f getPlayerPos() { return playerPos; }
	public void setPlayerPos(Vector2f aPos) { playerPos = aPos; }
	
	private LinkedList<VoicesTextureObjectReference> objectRefs = new LinkedList<VoicesTextureObjectReference>();
	public LinkedList<VoicesTextureObjectReference> getObjectRefs() { return objectRefs; }
	
	public VoicesLevelTemplate(String aName) 
	{
		super(aName, 0);
		playerPos = new Vector2f(0, 64);
	}
	public VoicesLevelTemplate(VoicesLevel lvl)
	{
		super(lvl.getTemplateName(), lvl.getTemplateId());
		playerPos = lvl.getPlayer().position.getPixel();
		for (VoicesTextureObject o : lvl.getTextureObjects())
		{
			if (!o.getClass().getName().equals(VoicesPlayer.class.getName()))
			{
				objectRefs.add(new VoicesTextureObjectReference(o));
			}
		}
	}

	public void writeObject(ObjectOutputStream out) throws IOException 
	{
		out.writeObject(getName());
		out.writeInt(getId());
		out.writeObject(playerPos);
		out.writeObject(objectRefs);
	}

	@SuppressWarnings("unchecked")
	public void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException 
	{
		setName((String)in.readObject());
		setId(in.readInt());
		playerPos = (Vector2f)in.readObject();
		objectRefs = (LinkedList<VoicesTextureObjectReference>)in.readObject();
	}

	public void readObjectNoData() throws ObjectStreamException 
	{
		//See VoicesTextureObject
	}
	
	public void saveToFile()
	{
		super.saveToFile("level");
	}
	
	public static VoicesLevelTemplate loadFromFile(String aFile)
	{
		return (VoicesLevelTemplate)VoicesTemplate.loadFromFile(aFile, "level");
	}
	
	public void delete()
	{
		super.delete("level");
	}
}
