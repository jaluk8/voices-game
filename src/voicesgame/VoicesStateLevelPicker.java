package voicesgame;

import java.util.LinkedList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesStateLevelPicker extends VoicesTemplatePicker 
{
	public VoicesStateLevelPicker(int s)
	{
		super(s);
	}

	public void chooseTemplate()
	{
		try
		{
			VoicesLevelTemplate vlt = (VoicesLevelTemplate)chosenTemplate;
			VoicesGameBoard.getInstance().setLevelTemplate(vlt);
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_EDITOR);
		setMode(MODE_NORMAL);
	}
	
	public void editTemplate()
	{
		try
		{
			VoicesLevelTemplate vlt = (VoicesLevelTemplate)chosenTemplate;
			VoicesGameBoard.getInstance().setLevelTemplate(vlt);
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_EDITOR_ADDER);
	}
	
	public void removeTemplate()
	{
		getTemplates().remove(chosenTemplate);
		VoicesGameBoard.getInstance().getLevelTemplates().remove(chosenTemplate);
		chosenTemplate.delete("level");
		setMode(MODE_NORMAL);
		createButtons();
	}
	
	public void addTemplate()
	{
		try
		{
			VoicesGameBoard.getInstance().setLevelTemplate(null);
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_EDITOR_ADDER);
	}

	public void enter(GameContainer gc, StateBasedGame sbg)
	{
		try
		{
			init(gc, sbg);
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		loadTemplates();
	}
	
	public void loadTemplates()
	{
		VoicesGameBoard.getInstance().loadLevelTemplates();
		setTemplates(new LinkedList<VoicesTemplate>());
		for (VoicesTemplate t : VoicesGameBoard.getInstance().getLevelTemplates())
		{
			getTemplates().add(t);
		}
		createButtons();
	}
}