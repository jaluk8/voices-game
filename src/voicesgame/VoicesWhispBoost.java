package voicesgame;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesWhispBoost extends VoicesWhisp
{
	static final float boostSpeed = 1f;
	static final float burstDistance = 64;
	
	public VoicesWhispBoost(Vector2f aPos, int anId, int aTeam)
	{
		super(aPos, Color.blue, anId, aTeam);
	}
	public VoicesWhispBoost(Vector2f aPos, int anId)
	{
		super(aPos, Color.blue, anId);
	}

	public VoicesTextureObject makeCopy()
	{
		return new VoicesWhispBoost(position.getPixel().copy(), getTemplateId(), team);
	}

	public void boost()
	{
		if (getMode() != MODE_DEATH)
		{
			Vector2f targetPos = targetPosition();
			Vector2f pos = VoicesGameBoard.getMousePosition().getPixel().sub(targetPos);
			double boostTheta = pos.getTheta();
			System.out.println(pos);
			targetOffset = new Vector2f(0, burstDistance);
			targetOffset.setTheta(boostTheta + 180);
			targetOffset.add(new Vector2f(target.size.getPixel().x / 2, target.size.getPixel().y / 2));
			
			if (target instanceof VoicesEntity)
			{
				VoicesEntity entityTarget = (VoicesEntity) target;
				Vector2f boostVel = new Vector2f(0, boostSpeed);
				boostVel.setTheta(boostTheta);
				entityTarget.velocity = boostVel;
				entityTarget.boosting = true;
			}
			
			setMode(MODE_DEATH);
		}
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException 
	{
		super.update(gc, sbg, delta);
	}
}
