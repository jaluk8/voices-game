package voicesgame;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.util.LinkedList;

import org.newdawn.slick.geom.Vector2f;

public class VoicesTextureObjectTemplate extends VoicesTemplate
{
	private static final long serialVersionUID = 3183853983272838366L;
	
	private String type;
	public String getType() { return type; }
	public void setType(String aName) { type = aName; }
	
	private String textureFolderName;
	public String getTextureFolderName() { return textureFolderName; }
	public void setTextureFolderName(String aName) { textureFolderName = aName; }
	
	private Vector2f size;
	public Vector2f getSize() { return size; }
	public void setSize(Vector2f aSize) { size = aSize; }
	
	private boolean translucent;
	public boolean isTranslucent() { return translucent; }
	public void setTranslucent(boolean i) { translucent = i; }
	
	private LinkedList<VoicesWall> walls = new LinkedList<VoicesWall>();
	public LinkedList<VoicesWall> getWalls() { return walls; }
	public void setWalls(LinkedList<VoicesWall> someWalls) { walls = someWalls; }
	
	public VoicesTextureObjectTemplate(String aName, String aFileName, String aType, int id) 
	{
		super(aName, id);
		textureFolderName = aFileName;
		type = aType;
	}
	public VoicesTextureObjectTemplate(VoicesTextureObject o) 
	{
		super(o.getTemplateName(), o.getTemplateId());
		textureFolderName = o.getTextureFolderName();
		size = o.size.getPixel();
		type = o.getClass().getName();
		translucent = o.isTranslucent();
		try
		{
			VoicesObstacle o2 = (VoicesObstacle) o; //Needed to breathe
			walls = o2.getWalls();
		}
		catch(Exception e)
		{
			//Not an obstacle
		}
	}
	
	public VoicesTextureObjectTemplate makeCopy()
	{
		VoicesTextureObjectTemplate copy = new VoicesTextureObjectTemplate(getName(), textureFolderName, type, 0);
		copy.walls = walls;
		translucent = copy.isTranslucent();
		return copy;
	}
	
	public void writeObject(ObjectOutputStream out) throws IOException 
	{
		out.writeObject(getName());
		out.writeInt(getId());
		out.writeObject(textureFolderName);
		out.writeBoolean(translucent);
		out.writeObject(walls);
	}

	@SuppressWarnings("unchecked")
	public void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException 
	{
		setName((String)in.readObject());
		setId(in.readInt());
		textureFolderName = (String)in.readObject();
		translucent = in.readBoolean();
		walls = (LinkedList<VoicesWall>)in.readObject();
	}

	public void readObjectNoData() throws ObjectStreamException 
	{
		//I honestly have no idea what this function is supposed to do but I need it...
	}
	
	public void saveToFile()
	{
		super.saveToFile("textureObject");
	}
	
	public static VoicesTextureObjectTemplate loadFromFile(String aFile)
	{
		return (VoicesTextureObjectTemplate)VoicesTemplate.loadFromFile(aFile, "textureObject");
	}
	
	public void delete()
	{
		super.delete("textureObject");
	}
}
