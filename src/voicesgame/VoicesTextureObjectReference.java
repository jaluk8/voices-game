package voicesgame;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;

import org.newdawn.slick.geom.Vector2f;

public class VoicesTextureObjectReference implements Serializable
{
	private static final long serialVersionUID = -7840148188929946331L;
	
	private int id;
	public int getId() { return id; }
	public void setId(int anId) { id = anId; }
	
	private Vector2f position;
	public Vector2f getPosition() { return position; }
	public void setPosition(Vector2f aPos) { position = aPos; }
	
	public VoicesTextureObjectReference(VoicesTextureObject vto)
	{
		id = vto.getTemplateId();
		position = vto.position.getPixel();
	}
	
	public VoicesTextureObject createVoicesTextureObject(VoicesLevel lvl)
	{
		VoicesTextureObjectTemplate aTemp;
		if (id > 0 && id <= VoicesGameBoard.getInstance().getSpecialTemplates().size())
		{
			aTemp = (VoicesTextureObjectTemplate) VoicesGameBoard.getInstance().getSpecialTemplates().get(id - 1);
		}
		else
		{
			aTemp = VoicesTextureObjectTemplate.loadFromFile(id + ".vtmp");
		}
		
		return VoicesTextureObjectFactory.createVoicesTextureObject(aTemp, position);
	}
	
	public void writeObject(ObjectOutputStream out) throws IOException 
	{
		out.writeInt(getId());
		out.writeObject(position);
	}

	public void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException 
	{
		setId(in.readInt());
		setPosition((Vector2f)in.readObject());
	}

	public void readObjectNoData() throws ObjectStreamException 
	{
		//I honestly have no idea what this function is supposed to do but I need it...
	}
}
