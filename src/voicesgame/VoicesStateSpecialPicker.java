package voicesgame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesStateSpecialPicker extends VoicesTemplatePicker
{
	public VoicesStateSpecialPicker(int s)
	{
		super(s);
		setMode(MODE_NO_EDIT);
	}

	public void chooseTemplate()
	{
		VoicesTextureObjectTemplate vlt = (VoicesTextureObjectTemplate)chosenTemplate;
		VoicesGameBoard.getInstance().setTextureObjectTemplate(vlt);
		
		VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_EDITOR);
	}
	
	public void editTemplate() { }
	
	public void removeTemplate() { }
	
	public void addTemplate() { }

	public void enter(GameContainer gc, StateBasedGame sbg)
	{
		try
		{
			init(gc, sbg);
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		loadTemplates();
	}
	
	public void loadTemplates()
	{
		if (getTemplates() == null)
		{
			setTemplates(VoicesGameBoard.getInstance().getSpecialTemplates());
		}
		
		createButtons();
	}
}
