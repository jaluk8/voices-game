package voicesgame;

import java.util.Stack;

import org.newdawn.slick.Color;
import org.newdawn.slick.geom.Vector2f;

public class VoicesStateLose extends VoicesScreenBasedState
{

	public VoicesStateLose(int s)
	{
		super(s);
		this.setBackgroundTextureName("lose");
		
		VoicesButton retryButton = new VoicesButton(new Vector2f(0, 512), new Vector2f(510, 64), "retry", Color.green, "Try again");
		addMenuObject(retryButton);

		VoicesButton exitButton = new VoicesButton(new Vector2f(516, 512), new Vector2f(510, 64), "brexit", Color.red, "Pull a Great Britain and leave");
		addMenuObject(exitButton);
	}
	
	protected void updateMenuEvents(Stack<VoicesMenuEvent> s)
	{
		for (VoicesMenuEvent e : s)
		{
			switch (e.getMenuObject().getName())
			{
			case "retry":
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_PLAY, VoicesStatePlay.MODE_REGULAR);
				break;
			case "brexit":
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_MAIN_MENU);
				break;
			}
		}
	}

	protected void updatePlayerEvents(Stack<VoicesPlayerEvent> s)
	{
		
	}

	protected void updateEditorEvents(Stack<VoicesEditorEvent> s)
	{
		
	}
}
