package voicesgame;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public abstract class VoicesMenuObject extends VoicesTextureObject
{
	private boolean selected;
	public boolean getSelected() { return selected; }
	public void setSelected(boolean aBool) { selected = aBool; }
	
	private boolean visibility;
	public boolean getVisibility() { return visibility; }
	public void setVisibility(boolean aBool) { visibility = aBool; }
	
	private String content = "";
	public String getContent() { return content; }
	public void setContent(String aContent)
	{
		content = aContent;
		try
		{
			this.scaleFont();
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
	}
	
	private Vector2f textOffset = new Vector2f(0, 0);
	
	protected String name;
	public String getName() { return name; }
	public void setName(String aName) { name = aName; }
	
	protected Color textColor;
	public Color getTextColor() { return textColor; }
	public void setTextColor(Color aColor) { textColor = aColor; }
	
	protected UnicodeFont font;
		
	public VoicesMenuObject(Vector2f aPos, Vector2f aSize, String aTextureName, String aName, Color aTextColor, String aContent)
	{
		super(aPos, aSize, aTextureName);
		name = aName;
		content = aContent;
		textColor = aTextColor;
		visibility = true;
		selected = false;
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g, boolean scroll) throws SlickException
	{
		if (visibility)
		{
			super.render(gc, sbg, g, scroll);
			
			g.setColor(textColor);
			g.setFont(font);
			g.drawString(content, position.getVirtual().x + textOffset.x, position.getVirtual().y + textOffset.y);
			
			if (selected)
			{
				Vector2f virtualPos = position.getVirtual();
				if (scroll)
				{
					virtualPos = VoicesGameBoard.getInstance().scrollVector(virtualPos);
				}
				Vector2f virtualSize = size.getVirtual();
				g.drawRect(virtualPos.x, virtualPos.y, virtualSize.x, virtualSize.y);
				
			}
		}
	}
	
	public void scaleFont() throws SlickException
	{
		font = VoicesGameBoard.getInstance().getDefaultFont();
		double width = font.getWidth(content);
		double height = font.getHeight(content);
		double intendedWidth = size.getVirtual().x;
		double intendedHeight = size.getVirtual().y;
		
		double fontSizeFromX = VoicesGameBoard.getInstance().DEFAULT_FONT_SIZE * intendedWidth / width;
		double fontSizeFromY = VoicesGameBoard.getInstance().DEFAULT_FONT_SIZE * intendedHeight / height;
		
		double chosenFontSize;
		if (fontSizeFromX < fontSizeFromY)
		{
			chosenFontSize = fontSizeFromX;
		}
		else
		{
			chosenFontSize = fontSizeFromY;
		}
		
		font = VoicesGameBoard.getInstance().generateFont((int)chosenFontSize);
		
		scaleTextOffset();
	}
	
	private void scaleTextOffset()
	{
		float textWidth = font.getWidth(content);
		float textHeight = font.getHeight(content);
		float boxWidth = size.getVirtual().x;
		float boxHeight = size.getVirtual().y;
		
		float xDiff = (boxWidth - textWidth) / 2;
		float yDiff = (boxHeight - textHeight) / 2;
		
		textOffset = new Vector2f(xDiff, yDiff);
	}
	
	public void rescaleVirtualVectors() throws SlickException
	{
		super.rescaleVirtualVectors();
		scaleFont();
	}
	
	public void mouseReleased(int button, int x, int y)
	{
		if (isHit(new Vector2f(x, y), getMode() != MODE_ADDER))
		{
			listener.addMenuEvent(this, button);
		}
	}
}
