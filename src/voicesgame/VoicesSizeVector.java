package voicesgame;

import org.newdawn.slick.geom.Vector2f;

public class VoicesSizeVector
{
	public boolean isNull = true;
	
	private Vector2f pixel;
	public Vector2f getPixel() { return pixel; }
	public void setPixel(Vector2f aSize)
	{
		pixel = aSize;
		rescaleVirtuals();
		isNull = false;
	}
	private Vector2f virtual;
	public Vector2f getVirtual() { return virtual; }
	public void setVirtual(Vector2f size)
	{
		virtual = size;
		rescalePixels();
		isNull = false;
	}
	
	public VoicesSizeVector()
	{
		
	}
	
	public VoicesSizeVector(Vector2f size, boolean isVirtual)
	{
		if (isVirtual)
		{
			setVirtual(size);
		}
		else
		{
			setPixel(size);
		}
		isNull = false;
	}
	
	public void rescaleVirtuals()
	{
		virtual = VoicesGameBoard.getInstance().scaleVector(pixel);
	}
	
	public void rescalePixels()
	{
		pixel = VoicesGameBoard.getInstance().antiScaleVector(virtual);
	}
}
