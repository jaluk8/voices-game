package voicesgame;

import java.util.LinkedList;

import org.lwjgl.input.Keyboard;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesPlayer extends VoicesEntity
{
	static final int MODE_NORMAL = 0;
	static final int MODE_R = 1;
	static final int MODE_L = 2;
	static final int MODE_WIN = 4;
		
	static final float WALK_ACCELERATION = 0.05f;
	static final float MAX_WALK_SPEED = 0.5f;
	static final float JUMP_SPEED = 0.5f;
	static final float BRAKE = 0.1f;
	
	public VoicesPlayer(Vector2f aPos) throws SlickException 
	{
		super(aPos, "res/entities/player", 0, VoicesEntity.TEAM_PLAYER);
	}
	
	public VoicesTextureObject makeCopy()
	{
		VoicesPlayer p2 = null;
		
		try
		{
			p2 = new VoicesPlayer(position.getPixel().copy());
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		
		return p2;
	}
	
	public void setWhisps(LinkedList<VoicesWhisp> someWhisps)
	{
		super.setWhisps(someWhisps);
		updatePlayStateWhispHud();
	}
	public void addWhisp(VoicesWhisp w)
	{
		super.addWhisp(w);
		updatePlayStateWhispHud();
	}
	
	public void updatePlayStateWhispHud()
	{
		VoicesGame g = VoicesGameBoard.getInstance().game;
		VoicesStatePlay p = (VoicesStatePlay)g.getState(VoicesGame.STATE_PLAY);
		p.updateHudWhisps();
	}
	
	public void moveToLevelConstraints(Vector2f limit)
	{
		//Fall through the bottom of levels
		if (this.position.getPixel().y > VoicesGameBoard.getInstance().PIXELS_PER_SCREEN_HEIGHT)
		{
			setMode(MODE_DEATH);
			this.setVelocity(new Vector2f(0, -JUMP_SPEED));
		}
		
		//Can't pass through the left side of a level
		if (this.position.getPixel().x < 0)
		{
			this.velocity.x = 0;
			this.position.getPixel().x = 0;
		}
		
		//Can't pass through the right either
		if (this.position.getPixel().x + this.size.getPixel().x > limit.x)
		{
			this.velocity.x = 0;
			this.position.getPixel().x = limit.x - this.size.getPixel().x;
		}
		
		//YOU SHALL NOT PASS through the top of a level because that would possibly make for awkward gameplay where the player skips most of a level by walking on its ceiling.
		if (this.position.getPixel().y < limit.y)
		{
			this.velocity.y = 0;
			this.position.getPixel().y = limit.y;
		}
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException
	{
		if (getMode() == MODE_DEATH)
		{
			if (sourceAnimation.isDone())
			{
				this.listener.addPlayerEvent(this, VoicesPlayerEvent.EVENT_DEATH);
			}
		}
		else if (getMode() == MODE_WIN)
		{
			if (sourceAnimation.isDone())
			{
				this.listener.addPlayerEvent(this, VoicesPlayerEvent.EVENT_WIN);
			}
		}
		else if (getMode() == MODE_L)
		{
			if (velocity.x > -MAX_WALK_SPEED)
			{
				velocity.x = Math.max(-MAX_WALK_SPEED, velocity.x - WALK_ACCELERATION);
			}
		}
		else if (getMode() == MODE_R)
		{
			if (velocity.x < MAX_WALK_SPEED)
			{
				velocity.x = Math.min(MAX_WALK_SPEED, velocity.x + WALK_ACCELERATION);
			}
		}
		else if (!boosting)
		{
			if (velocity.x > 0)
			{
				velocity.x = Math.max(0, velocity.x - BRAKE);
			}
			else if (velocity.x < 0)
			{
				velocity.x = Math.min(0, velocity.x + BRAKE);
			}
		}
		
		if (onTheRight || onTheLeft)
		{
			velocity.x = 0;
		}
		
		super.update(gc, sbg, delta);
	}
	
	public void loadTextureFromMode()
	{
		String filePath = getTextureFolderName();
		boolean loop = true;
		switch(getMode())
		{
		case MODE_NORMAL:
			if (onTheFloor)
			{
				filePath += "/normal";
			}
			else
			{
				filePath += "/jump";
				loop = false;
			}
			break;
		case MODE_R:
			filePath += "/walkR";
			break;
		case MODE_L:
			filePath += "/walkL";
			break;
		case MODE_IN_EDITOR:
			filePath += "/normal";
			break;
		case MODE_ADDER:
			filePath += "/normal";
			break;
		case MODE_PREVIEW:
			filePath = "res/entities/player/jump";
			break;
		case MODE_DEATH:
			filePath = "res/entities/player/death";
			loop = false;
			break;
		case MODE_WIN:
			filePath = "res/entities/player/win";
			loop = false;
			break;
		}
		setSourceAnimation(filePath);
		sourceAnimation.setLooping(loop);
	}
	
	public void ableToWin()
	{
		if (onTheFloor)
		{
			setMode(MODE_WIN);
			velocity = new Vector2f(0, 0);
		}
	}
	
	public void hit()
	{
		setMode(MODE_DEATH);
	}
	
	public void keyPressed(int key, char c) throws SlickException
	{
		if (getMode() != MODE_DEATH && getMode() != MODE_WIN)
		{
			if (key == Keyboard.KEY_A && getMode() == MODE_NORMAL)
			{
				setMode(MODE_L);
			}
			else if (key == Keyboard.KEY_D && getMode() == MODE_NORMAL)
			{
				setMode(MODE_R);
			}
			else if (key == Keyboard.KEY_W && onTheFloor)
			{
				if (onTheFloor)
				{
					onTheFloor = false;
					resetMode();
				}
				this.velocity.y -= JUMP_SPEED;
			}
			else if (key == Keyboard.KEY_SPACE)
			{
				VoicesWhispBoost boost = (VoicesWhispBoost) getFirstWhispOfType(VoicesWhispBoost.class, false);
				if (boost != null)
				{
					boost.boost();
				}
			}
		}
	}
	
	public void mousePressed(int button, int x, int y)
	{
		if (getMode() != MODE_DEATH && getMode() != MODE_WIN)
		{
			VoicesTextureObject hit = null;
			for (VoicesTextureObject vto : VoicesGameBoard.getCurrentLevel().getTextureObjects())
			{
				if (vto.isHit(VoicesGameBoard.getMousePosition().getVirtual(), false))
				{
					hit = vto;
				}
			}
			
			if (hit instanceof VoicesObstacle)
			{
				VoicesObstacle o = (VoicesObstacle) hit;
				VoicesWhispBridge bridge = (VoicesWhispBridge) getFirstWhispOfType(VoicesWhispBridge.class, false);
				if (bridge != null && o.isTranslucent())
				{
					bridge.bridge(o);
				}
			}
			
			if (hit instanceof VoicesEntity)
			{
				VoicesEntity hitEnt = (VoicesEntity) hit;
				VoicesWhispAttack attack = (VoicesWhispAttack) getFirstWhispOfType(VoicesWhispAttack.class, false);
				if (attack != null && hitEnt.team != team)
				{
					attack.attack(hitEnt);
				}
			}
		}
	}
	
	public void keyReleased(int key, char c)
	{
		if (getMode() != MODE_DEATH && getMode() != MODE_WIN)
		{
			if (key == Keyboard.KEY_A && getMode() == MODE_L)
			{
				setMode(MODE_NORMAL);
			}
			else if (key == Keyboard.KEY_D && getMode() == MODE_R)
			{
				setMode(MODE_NORMAL);
			}
		}
	}
}
