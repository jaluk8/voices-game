package voicesgame;

import java.util.LinkedList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesStateTextureObjectPicker extends VoicesTemplatePicker 
{
	public VoicesStateTextureObjectPicker(int s)
	{
		super(s);
	}

	public void chooseTemplate()
	{
		VoicesTextureObjectTemplate vtot = (VoicesTextureObjectTemplate)chosenTemplate;
		VoicesGameBoard.getInstance().setTextureObjectTemplate(vtot);
		VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_EDITOR);
		setMode(MODE_NORMAL);
	}
	
	public void editTemplate()
	{
		VoicesTextureObjectTemplate vtot = (VoicesTextureObjectTemplate)chosenTemplate;
		VoicesGameBoard.getInstance().setTextureObjectTemplate(vtot);
		VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_TEXTURE_OBJECT_ADDER);	
	}
	
	public void removeTemplate()
	{
		getTemplates().remove(chosenTemplate);
		VoicesGameBoard.getInstance().getTextureObjectTemplates().remove(chosenTemplate);
		chosenTemplate.delete("textureObject");
		setMode(MODE_NORMAL);
		createButtons();
	}
	
	public void addTemplate()
	{
		VoicesGameBoard.getInstance().setTextureObjectTemplate(null);
		VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_TEXTURE_OBJECT_ADDER);		
	}

	public void enter(GameContainer gc, StateBasedGame sbg)
	{
		try
		{
			init(gc, sbg);
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		loadTemplates();
	}
	
	public void loadTemplates()
	{
		VoicesGameBoard.getInstance().loadTextureObjectTemplates();
		setTemplates(new LinkedList<VoicesTemplate>());
		for (VoicesTemplate t : VoicesGameBoard.getInstance().getTextureObjectTemplates())
		{
			getTemplates().add(t);
		}
		createButtons();
	}
}