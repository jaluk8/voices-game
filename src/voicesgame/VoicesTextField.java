package voicesgame;

import org.newdawn.slick.Color;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;

public class VoicesTextField extends VoicesMenuObject 
{
	public final int MODE_NORMAL = 0;
	public final int MODE_WRITING = 1;
	
	public VoicesTextField(Vector2f aPos, Vector2f aSize, String aName, Color aTextColor, String aContent) 
	{
		super(aPos, aSize, "res/menu/textField", aName, aTextColor, aContent);
	}
	
	public VoicesTextureObject makeCopy()
	{
		VoicesTextField tf2 = new VoicesTextField(position.getPixel().copy(), size.getPixel().copy(), getName(), getTextColor(), getContent());
		return tf2;
	}

	public void mousePressed(int button, int x, int y) 
	{
		Vector2f pos = new Vector2f(x, y);
		if (isHit(pos, false))
		{
			setMode(MODE_WRITING);
		}
		else
		{
			if (getMode() == MODE_WRITING)
			{
				listener.addMenuEvent(this, button);
			}
			setMode(MODE_NORMAL);
		}
	}
	
	public void keyPressed(int aKey, char aChar) throws SlickException 
	{
		if (getMode() == MODE_WRITING)
		{
			int key = aChar;
			if (key == 8 && getContent().length() > 0)
			{
				setContent(getContent().substring(0, getContent().length() - 1));
			}
			else if (key == 13)
			{
				listener.addMenuEvent(this, 13);
			}
			else if (key >= 32 && key <= 126)
			{
				setContent(getContent() + aChar);
			}
			rescaleVirtualVectors();
		}
	}
	
	public void setMode (int aMode)
	{
		super.setMode(aMode);
		setSelected(getMode() == MODE_WRITING);
	}
	
	public void loadTextureFromMode()
	{
		String filePath = getTextureFolderName();
		switch(getMode())
		{
		case MODE_NORMAL:
			filePath += "/normal";
			break;
		case MODE_WRITING:
			filePath += "/writing";
			break;
		}
		setSourceAnimation(filePath);
	}
}
