package voicesgame;

import java.util.Stack;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesStateLevelAdder extends VoicesState
{
	private VoicesLevelTemplate tmp;
	
	private VoicesTextField nameField;
	private VoicesTextBox outputBox;
	private VoicesButton saveButton;
	
	public VoicesStateLevelAdder(int s)
	{
		super(s);
		
		addMenuObject(new VoicesTextBox(new Vector2f(0, 0), new Vector2f(508, 64), "info", Color.gray, "Level Name:"));
		
		outputBox = new VoicesTextBox(new Vector2f(0, 72), new Vector2f(508, 64), "info", Color.gray, "");
		nameField = new VoicesTextField(new Vector2f(512, 0), new Vector2f(508, 64), "title", Color.green, "");
		saveButton = new VoicesButton(new Vector2f(512, 72), new Vector2f(508, 64), "save", Color.green, "Save");
		
		addMenuObject(nameField);
		addMenuObject(outputBox);
		addMenuObject(saveButton);
	}
	
	protected void updateMenuEvents(Stack<VoicesMenuEvent> s)
	{
		while (s.size() > 0)
		{
			VoicesMenuEvent e = s.pop();
			VoicesMenuObject source = e.getMenuObject();
			
			tmp.setName(nameField.getContent());
			if (source == saveButton)
			{
				tmp.saveToFile();
				VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_LEVEL_PICKER);
			}
		}
	}

	protected void updatePlayerEvents(Stack<VoicesPlayerEvent> s)
	{
		
	}

	protected void updateEditorEvents(Stack<VoicesEditorEvent> s)
	{
		
	}
	
	public void enter(GameContainer gc, StateBasedGame sbg) throws SlickException
	{
		super.enter(gc, sbg);
		
		tmp = VoicesGameBoard.getInstance().getLevelTemplate();
		if (tmp == null)
		{
			tmp = new VoicesLevelTemplate("");
			outputBox.setContent("Level Created");
			nameField.setContent("");
		}
		else
		{
			outputBox.setContent("Level loaded");
			nameField.setContent(tmp.getName());
		}
	}

}
