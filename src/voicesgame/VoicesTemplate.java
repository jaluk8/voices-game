package voicesgame;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamException;
import java.io.Serializable;

public abstract class VoicesTemplate implements Serializable
{
	private static final long serialVersionUID = 7778037959631778318L;
	
	private String name;
	public String getName() { return name; }
	public void setName(String aName) { name = aName; }
	
	private int id;
	public int getId() { return id; }
	public void setId(int anId) { id = anId; }
	
	public VoicesTemplate(String aName, int anId)
	{
		name = aName;
		if(anId == 0)
		{
			id = Math.abs(VoicesGameBoard.getInstance().getRandom().nextInt());
		}
		else
		{
			id = anId;
		}
	}
	
	protected void saveToFile(String type)
	{
		try
		{
			FileOutputStream fout = new FileOutputStream("templates/" + type + "/" + getId() + ".vtmp");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(this);
			oos.close();
			fout.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	protected static Object loadFromFile(String aFile, String type)
	{
		try
		{
			FileInputStream fin = new FileInputStream("templates/" + type + "/" + aFile);
			ObjectInputStream ois = new ObjectInputStream(fin);
			Object out = ois.readObject();
			ois.close();
			fin.close();
			return out;
		} 
		catch (IOException | ClassNotFoundException e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	protected void delete(String type)
	{
		File f = new File("templates/" + type + "/" + getId() + ".vtmp");
		f.delete();
	}
	
	public abstract void writeObject(ObjectOutputStream out) throws IOException;
	public abstract void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException;
	public abstract void readObjectNoData() throws ObjectStreamException;
}
