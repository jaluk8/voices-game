package voicesgame;

import java.util.LinkedList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public class VoicesStatePlayPicker extends VoicesTemplatePicker 
{
	public VoicesStatePlayPicker(int s)
	{
		super(s);
		setMode(MODE_NO_EDIT);
	}

	public void chooseTemplate()
	{
		try
		{
			VoicesLevelTemplate vlt = (VoicesLevelTemplate)chosenTemplate;
			VoicesGameBoard.getInstance().setLevelTemplate(vlt);
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		
		VoicesGameBoard.getInstance().game.enterState(VoicesGame.STATE_PLAY, VoicesStatePlay.MODE_REGULAR);
	}
	
	public void editTemplate() { }
	
	public void removeTemplate()
	{
		
	}
	
	public void addTemplate()
	{
		
	}

	public void enter(GameContainer gc, StateBasedGame sbg)
	{
		try
		{
			init(gc, sbg);
		}
		catch (SlickException e)
		{
			e.printStackTrace();
		}
		loadTemplates();
	}
	
	public void loadTemplates()
	{
		VoicesGameBoard.getInstance().loadLevelTemplates();
		setTemplates(new LinkedList<VoicesTemplate>());
		for (VoicesTemplate t : VoicesGameBoard.getInstance().getLevelTemplates())
		{
			getTemplates().add(t);
		}
		createButtons();
	}
}