package voicesgame;

import org.lwjgl.opengl.Display;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

public abstract class VoicesScreenBasedState extends VoicesState
{
	private String backgroundTextureName;
	protected void setBackgroundTextureName(String aName)
	{
		backgroundTextureName = "res/screens/" + aName;
	}
	
	private VoicesAnimation backgroundAnimation = new VoicesAnimation(true);
	public void setBackgroundAnimation() 
	{
		backgroundAnimation.setTextures(backgroundTextureName);
	}
	
	public VoicesScreenBasedState(int s)
	{
		super(s);
	}
	
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException
	{
		super.init(gc, sbg);
		backgroundAnimation.setTextures(backgroundTextureName);
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException
	{
		float width = VoicesGameBoard.getInstance().PIXELS_PER_SCREEN_WIDTH;
		float height = VoicesGameBoard.getInstance().PIXELS_PER_SCREEN_HEIGHT;
		
		g.drawImage(backgroundAnimation.getTexture(), 0, 0, Display.getWidth(), Display.getHeight(), 0, 0, width, height);
		
		super.render(gc, sbg, g);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException
	{
		super.update(gc, sbg, delta);
		backgroundAnimation.advance(delta);
	}
}
